#pragma once

#include "test_sdmatrix.h"

#include "test_lp1d.h"
#include "test_lp2d.h"
#include "test_lp3d.h"

void test_all()
{
    test_sdmatrix();
    
    test_lp_1d();
    test_lp_2d();
    test_lp_3d();

    
}