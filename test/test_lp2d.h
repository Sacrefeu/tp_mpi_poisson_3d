#pragma once

#include <cmath>

#include "../PDE/Laplacian.h"
#include "../PDE/DirichletConditionsGrid.h"

double f(double x, double y)
{
    return -(std::cos(x) + std::sin(y));
}

double y_exact(double x, double y)
{
    return std::cos(x) + std::sin(y);
}

// Note : #define est requis ici pour le calcul des bordures
// Selon le standard c++, une lambda qui capture ne peut pas
// �tre convertie en pointeur sur fonction
#define ax 0.0
#define bx 1.0
#define ay 0.0
#define by 2.0

void test_lp_2d()
{
    const std::size_t Nx = 75;
    const std::size_t Ny = 75;

    const double hx = (bx - ax) / (Nx - 1);
    const double hy = (by - ay) / (Ny - 1);

    Grid2D<double> grid(
        Axis<double>(Nx, ax + hx, bx - hx),
        Axis<double>(Ny, ay + hy, by - hy)
    );
    const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());
    const double ihy2 = 1.0 / (grid.gethy() * grid.gethy());
    
    SDMatrix<double> lp = getLaplacian2D(&grid);

    DirichletConditionsGrid2D<double> conditions(&grid);
    conditions.applyOnLinearOperator(lp);

    Vector<double> b_f   = grid.apply(f);
    Vector<double> exact = grid.apply(y_exact);

    Vector<double> left = grid.border("left").apply([](double x) { return y_exact(ax, x); });
    Vector<double> back = grid.border("back").apply([](double x) { return y_exact(x, ay); });
    Vector<double> right = grid.border("right").apply([](double x) { return y_exact(bx, x); });
    Vector<double> front = grid.border("front").apply([](double x) { return y_exact(x, by); });
    conditions.setCondition("left" , left , -ihx2);
    conditions.setCondition("right", right, -ihx2);
    conditions.setCondition("front", front, -ihy2);
    conditions.setCondition("back" , back , -ihy2);

    conditions.applyOnRhs(b_f);

    Vector<double> x(grid.getPointCount());
    GC(lp, b_f, x, 1e-10, 100000);

    Vector<double> error = exact - x;
    std::cout << "Error : " << scal(error, error) << std::endl;

    toCSVFile("tests_lp2D_approx.csv", x);
    toCSVFile("tests_lp2D_exacts.csv", exact);
}