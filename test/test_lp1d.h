#pragma once

#include <cmath>

#include "../PDE/Laplacian.h"
#include "../PDE/DirichletConditionsGrid.h"

double f(double x)
{
    return std::cos(x);
}

double y_exact(double x)
{
    return -std::cos(x) + (std::cos(10.0) - 3) * x / 10.0 + 2;
}

void test_lp_1d()
{
    const std::size_t N = 3000;
    const double a = 0.0, b = 10.0;
    const double hx = (b - a) / (N - 1);

    Grid1D<double> grid(Axis<double>(N, a + hx, b - hx));

    SDMatrix<double> lp = getLaplacian1D(&grid);
    DirichletConditionsGrid1D<double> conditions(&grid);
    conditions.applyOnLinearOperator(lp);

    Vector<double> b_f = grid.apply(f);
    Vector<double> exact = grid.apply(y_exact);

    const double ihx2 = 1.0 / (grid.getStep(0) * grid.getStep(0));
    conditions.setCondition("left" , { y_exact(a) }, -ihx2 );
    conditions.setCondition("right", { y_exact(b) }, -ihx2 );

    conditions.applyOnRhs(b_f);

    Vector<double> x(grid.getPointCount());
    GC(lp, b_f, x, 1e-10);

    Vector<double> error = exact - x;
    std::cout << "Error : " << scal(error, error) << std::endl;

    toCSVFile("tests_lp1D_approx.csv", x);
    toCSVFile("tests_lp1D_exacts.csv", exact);
}