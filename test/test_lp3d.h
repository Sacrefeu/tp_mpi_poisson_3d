#pragma once

#include <cmath>

#include "../PDE/Laplacian.h"
#include "../PDE/DirichletConditionsGrid.h"

double f(double x, double y, double z)
{
    const double cos_z = std::cos(z);
    return -std::cos(x) - std::sin(y) + 2 * std::tan(z) / (cos_z * cos_z);
}

double y_exact(double x, double y, double z)
{
    return std::cos(x) + std::sin(y) + std::tan(z);
}

#define ax 0.0
#define bx 1.0
#define ay 0.25
#define by 1.5
#define az 0.5
#define bz 1.0

void test_lp_3d()
{
    const std::size_t Nx = 50;
    const std::size_t Ny = 50;
    const std::size_t Nz = 50;

    const double hx = (bx - ax) / (Nx - 1);
    const double hy = (by - ay) / (Ny - 1);
    const double hz = (bz - az) / (Nz - 1);

    Grid3D<double> grid(
        Axis<double>(Nx, ax + hx, bx - hx),
        Axis<double>(Ny, ay + hy, by - hy),
        Axis<double>(Nz, az + hz, bz - hz)
    );

    const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());
    const double ihy2 = 1.0 / (grid.gethy() * grid.gethy());
    const double ihz2 = 1.0 / (grid.gethz() * grid.gethz());

    SDMatrix<double> lp = getLaplacian3D(&grid);

    DirichletConditionsGrid3D<double> conditions(&grid);
    
    Vector<double> b_f   = grid.apply(f);
    Vector<double> exact = grid.apply(y_exact);

    Vector<double> left  = grid.border("left") .apply([](double y, double z) { return y_exact(ax, y, z); });
    Vector<double> right = grid.border("right").apply([](double y, double z) { return y_exact(bx, y, z); });
    Vector<double> back  = grid.border("back") .apply([](double x, double z) { return y_exact(x, ay, z); });
    Vector<double> front = grid.border("front").apply([](double x, double z) { return y_exact(x, by, z); });
    Vector<double> bot   = grid.border("bot")  .apply([](double x, double y) { return y_exact(x, y, az); });
    Vector<double> top   = grid.border("top")  .apply([](double x, double y) { return y_exact(x, y, bz); });

    conditions.setCondition("left" , left , -ihx2);
    conditions.setCondition("right", right, -ihx2);
    conditions.setCondition("front", front, -ihy2);
    conditions.setCondition("back" , back , -ihy2);
    conditions.setCondition("top"  , top  , -ihz2);
    conditions.setCondition("bot"  , bot  , -ihz2);
  
    conditions.applyOnLinearOperator(lp);
    conditions.applyOnRhs(b_f);

    Vector<double> x(grid.getPointCount());
    GC_Reporter<double> reporter = GC_Reporter_options::PRINT_ERRORS;
    GC(lp, b_f, x, 1e-10, 100000, &reporter);
   
    Vector<double> error = exact - x;
    std::cout << "Error : " << scal(error, error) << std::endl;

    toCSVFile("tests_lp3D_approx.csv", x);
    toCSVFile("tests_lp3D_exacts.csv", exact);
}