#pragma once

#include "../MPIBinding/DistributedMesh/DistributedCartesianMesh.h"
#include "../MPIBinding/VectorExchanger/SendReceiveVectorExchanger.h"
#include "../MPIBinding/VectorExchanger/RMAVectorExchanger.h"

void test_exchangers()
{
    DistributedGrid2D<double> mesh(
        Axis<double>(0, 0.0, 0.0), 
        Axis<double>(0, 0.0, 0.0)
    );

    SendReceiveVectorExchanger<double> leftExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("left"), mesh.getNeighbourMeshRank("right")
    );
    SendReceiveVectorExchanger<double> rightExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("right"), mesh.getNeighbourMeshRank("left")
    );
    SendReceiveVectorExchanger<double> frontExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("front"), mesh.getNeighbourMeshRank("back")
    );
    SendReceiveVectorExchanger<double> backExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("back"), mesh.getNeighbourMeshRank("front")
    );

    Vector<double> toSend = { (double)mesh.getRank(), (double)(mesh.getRank() * mesh.getRank()) };
    leftExchanger.setData(toSend);
    rightExchanger.setData(toSend);
    frontExchanger.setData(toSend);
    backExchanger.setData(toSend);

    leftExchanger.exchange();
    rightExchanger.exchange();
    frontExchanger.exchange();
    backExchanger.exchange();

    std::cout << "[" << mesh.getRank() << "] Received from right : " << leftExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from left  : " << rightExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from front : " << backExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from back  : " << frontExchanger.getData() << std::endl;

}
void test_naivermagetexchanger()
{
    DistributedGrid2D<double> mesh(
        Axis<double>(0, 0.0, 0.0),
        Axis<double>(0, 0.0, 0.0)
    );

    NaiveRMAGetVectorExchanger leftExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("left"), mesh.getNeighbourMeshRank("right")
    );
    NaiveRMAGetVectorExchanger rightExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("right"), mesh.getNeighbourMeshRank("left")
    );
    NaiveRMAGetVectorExchanger frontExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("front"), mesh.getNeighbourMeshRank("back")
    );
    NaiveRMAGetVectorExchanger backExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("back"), mesh.getNeighbourMeshRank("front")
    );

    Vector<double> toSend = { (double)mesh.getRank(), (double)(mesh.getRank() * mesh.getRank()) };
    leftExchanger.setData(toSend);
    rightExchanger.setData(toSend);
    frontExchanger.setData(toSend);
    backExchanger.setData(toSend);

    leftExchanger.sync();
    rightExchanger.sync();
    frontExchanger.sync();
    backExchanger.sync();

        leftExchanger.exchange();
        rightExchanger.exchange();
        frontExchanger.exchange();
        backExchanger.exchange();

    leftExchanger.sync();
    rightExchanger.sync();
    frontExchanger.sync();
    backExchanger.sync();

    std::cout << "[" << mesh.getRank() << "] Received from right : " << leftExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from left  : " << rightExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from front : " << backExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from back  : " << frontExchanger.getData() << std::endl;
}
void test_naivermaputexchanger()
{
    DistributedGrid2D<double> mesh(
        Axis<double>(0, 0.0, 0.0),
        Axis<double>(0, 0.0, 0.0)
    );

    NaiveRMAPutVectorExchanger leftExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("left"), mesh.getNeighbourMeshRank("right")
    );
    NaiveRMAPutVectorExchanger rightExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("right"), mesh.getNeighbourMeshRank("left")
    );
    NaiveRMAPutVectorExchanger frontExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("front"), mesh.getNeighbourMeshRank("back")
    );
    NaiveRMAPutVectorExchanger backExchanger(&mesh,
        2, mesh.getNeighbourMeshRank("back"), mesh.getNeighbourMeshRank("front")
    );

    Vector<double> toSend = { (double)mesh.getRank(), (double)(mesh.getRank() * mesh.getRank()) };
    leftExchanger.setData(toSend);
    rightExchanger.setData(toSend);
    frontExchanger.setData(toSend);
    backExchanger.setData(toSend);

    leftExchanger.sync();
    rightExchanger.sync();
    frontExchanger.sync();
    backExchanger.sync();

    leftExchanger.exchange();
    rightExchanger.exchange();
    frontExchanger.exchange();
    backExchanger.exchange();

    leftExchanger.sync();
    rightExchanger.sync();
    frontExchanger.sync();
    backExchanger.sync();

    std::cout << "[" << mesh.getRank() << "] Received from right : " << leftExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from left  : " << rightExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from front : " << backExchanger.getData() << std::endl;
    std::cout << "[" << mesh.getRank() << "] Received from back  : " << frontExchanger.getData() << std::endl;
}
void test_rmaputexchanger()
{
    DistributedGrid2D<double> mesh(
        Axis<double>(0, 0.0, 0.0),
        Axis<double>(0, 0.0, 0.0)
    );

    RMAPutVectorsExchanger exchanger(&mesh,
        {
            {"left" , { 2, { "right", mesh.getNeighbourMeshRank("left") } } },
            {"right", { 2, { "left" , mesh.getNeighbourMeshRank("right")  } } },
            {"front", { 2, { "back" , mesh.getNeighbourMeshRank("front")  } } },
            {"back" , { 2, { "front", mesh.getNeighbourMeshRank("back") } } }
        });

    exchanger.sync();

    Vector<double> toSend = { (double)mesh.getRank(), (double)(mesh.getRank() * mesh.getRank()) };
    for (const auto& border : { "left", "right", "front", "back" })
        exchanger.setData(border, toSend);

    exchanger.exchangeall();
    exchanger.sync();

    for (const auto& border : { "left", "right", "front", "back" })
    {
        std::cout << "[" << mesh.getRank() << "] Received in " << border << " : " << exchanger.getData(border)[0] << "," << exchanger.getData(border)[1] << std::endl;
    }
}
void test_rmagetexchanger()
{
    DistributedGrid2D<double> mesh(
        Axis<double>(0, 0.0, 0.0),
        Axis<double>(0, 0.0, 0.0)
    );

    RMAGetVectorsExchanger exchanger(&mesh,
        {
            {"left" , { 2, { "right", mesh.getNeighbourMeshRank("left")  } } },
            {"right", { 2, { "left" , mesh.getNeighbourMeshRank("right") } } },
            {"front", { 2, { "back" , mesh.getNeighbourMeshRank("front") } } },
            {"back" , { 2, { "front", mesh.getNeighbourMeshRank("back")  } } }
        });

    exchanger.sync();

    Vector<double> toSend = { (double)mesh.getRank(), (double)(mesh.getRank() * mesh.getRank()) };
    for (const auto& border : { "left", "right", "front", "back" })
        exchanger.setData(border, toSend);

    exchanger.exchangeall();
    exchanger.sync();

    for (const auto& border : { "left", "right", "front", "back" })
    {
        std::cout << "[" << mesh.getRank() << "] Received from " << border << " : " << exchanger.getData(border)[0] << "," << exchanger.getData(border)[1] << std::endl;
    }
}