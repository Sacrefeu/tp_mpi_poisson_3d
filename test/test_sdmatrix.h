#pragma once

#include "../Linalg/linalg.h"

void test_sdmatrix()
{
    SDMatrix<double> mat(6, 3);
    mat.setOffsets({ 0, 1, 5 });
    mat[0] = Vector<double>(6, 1.0);
    mat[1] = Vector<double>(5, 2.0);
    mat[2] = Vector<double>(1, 3.0);

    Vector<double> rhs = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };

    std::cout << (mat * rhs) << std::endl;
}