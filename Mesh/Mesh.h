#pragma once

#include "../Linalg/Vector.h"
#include <string>

/**
 * \brief 1D mesh interface
 * 
 * \tparam T Field (float, double, std::complex, ...)
 */
template<typename T>
class Mesh1D
{
public:
    /** 
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border 
     */
    inline virtual std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const = 0;

    /** 
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */
    inline virtual std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const = 0;
    
    /**
     * \brief Return the number of point in the domain
     * 
     * \return The number of point in the domain
     */
    inline virtual std::size_t getPointCount() const = 0;

    /**
     * \brief Return the i-th point of the domain
     * 
     * \param at The idex of the point
     * \param[out] x The coordinate of the point
     *
     */
    inline virtual void getPoint(std::size_t at, T& x) const = 0;
    
    /**
     * \brief Apply a function over the entire domain (boundary included)
     *
     * \param func The function to apply over the domain
     *
     * \return The list of points 
     */
    inline Vector<T> apply(T(*func)(T)) const
    {
        Vector<T> applied(getPointCount());

        T x;
        for (std::size_t i = 0; i < getPointCount(); i++)
        {
            getPoint(i, x);
            applied[i] = func(x);
        }

        return applied;
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points 
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, Vector<T>& to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, T* to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }

    /**
     * \brief Dctor
     */
    inline virtual ~Mesh1D()
    {
    }
private:
};

/**
 * \brief 2D mesh interface
 *
 * \tparam T Field (float, double, std::complex, ...)
 */
template<typename T>
class Mesh2D
{
public:
    /**
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border
     */
    inline virtual std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const = 0;

    /**
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */
    inline virtual std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const = 0;
  
    /**
     * \brief Return the number of point in the domain
     *
     * \return The number of point in the domain
     */
    inline virtual std::size_t getPointCount() const = 0;

    /**
     * \brief Return the i-th point of the domain
     *
     * \param at The idex of the point
     *
     * \param[out] x The x coordinate of the point
     * \param[out] y The y coordinate of the point
     */
    inline virtual void getPoint(std::size_t at, T& x, T& y) const = 0;
    
    /**
     * \brief Apply a function over the entire domain (boundary included)
     *
     * \param func The function to apply over the domain
     *
     * \return The list of points
     */
    inline Vector<T> apply(T(*func)(T, T)) const
    {
        Vector<T> applied(getPointCount());

        T x, y;
        for (std::size_t i = 0; i < getPointCount(); i++)
        {
            getPoint(i, x, y);
            applied[i] = func(x, y);
        }

        return applied;
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, Vector<T>& to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, T* to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }


    /**
     * \brief Dctor
     */
    inline virtual ~Mesh2D()
    {
    }
private:
};

/**
 * \brief 2D mesh interface
 *
 * \tparam T Field (float, double, std::complex, ...)
 */
template<typename T>
class Mesh3D
{
public:
    /**
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border
     */
    inline virtual std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const = 0;

    /**
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */
    inline virtual std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const = 0;

    /**
     * \brief Return the number of point in the domain
     *
     * \return The number of point in the domain
     */
    inline virtual std::size_t getPointCount() const = 0;

    /**
     * \brief Return the i-th point of the domain
     *
     * \param at The idex of the point
     *
     * \param[out] x The x coordinate of the point
     * \param[out] y The y coordinate of the point
     * \param[out] z The z coordinate of the point
     */
    inline virtual void getPoint(std::size_t at, T& x, T& y, T& z) const = 0;
    
    /**
     * \brief Apply a function over the entire domain (boundary included)
     *
     * \param func The function to apply over the domain
     *
     * \return The list of points
     */
    inline Vector<T> apply(T(*func)(T, T, T)) const
    {
        Vector<T> applied(getPointCount());

        T x, y, z;
        for (std::size_t i = 0; i < getPointCount(); i++)
        {
            getPoint(i, x, y, z);
            applied[i] = func(x, y, z);
        }

        return applied;
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, Vector<T>& to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }

    /**
     * \brief Gather points on a border
     *
     * \param border The name of the border
     * \param from The vector containing all points
     * \param [out] to The destination vector (must contain at least getBorderPointCount(border) elements)
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     */
    inline void gatherBorder(const std::string& border, const Vector<T>& from, T* to, std::size_t displacement = 0)
    {
        for (std::size_t i = 0; i < getBorderPointCount(border, displacement); i++)
        {
            to[i] = from[getBorderPoint(border, i, displacement)];
        }
    }


    /**
     * \brief Dctor 
     */
    inline virtual ~Mesh3D()
    {
    }
private:
};