#pragma once

#include "Mesh.h"

/**
 * \brief Discretized Axis
 *
 * \tparam T Field (float, double, std::complex, ...)
 */
template<typename T>
struct Axis
{
    /**
     * \brief Ctor
     *
     * \param d Number of point along the axis
     * \param s Start of the axis
     * \param e End of the axis
     */
    Axis(std::size_t d, T s, T e)
    {
        n = d;
        start = s;
        end = e;
    }

    /**
     * \brief Return the i-th point along the axis
     *
     * Note : 0 is the start and n-1 is the end
     * 
     * \param i The index of the point
     */
    T getPoint(std::size_t i) const
    {
        return start + i * (end - start) / (n - 1);
    }

    std::size_t n; /**< Number of point along this axis*/
    T start; /**< Start of the axis */
    T end;  /**< End of the axis */
};

/**
 * \brief 1D Grid
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class Grid1D : public Mesh1D<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param x Axis defining the grid
     */
    Grid1D(const Axis<T>& x) : xAxis(x)
    {
        
    }
    
    /**
     * \brief Return the number of point in the left border
     *
     * \return The number of point in the left border
     */
    std::size_t getLeftIndicesCount() const
    {
        return 1;
    }

    /**
     * \brief Return the i-th point of the left border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the left border
     */
    std::size_t getLeftIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return displacement;
    }

    /**
     * \brief Return the i-th point of the right border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the right border
     */
    std::size_t getRightIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return xAxis.n - 1 - displacement;
    }

    /**
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border
     */
    inline std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndicesCount();
        else if (borderName == "right")
            return getLeftIndicesCount();

        return 0;
    }

    /**
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */
    inline std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndex(i, displacement);
        else if (borderName == "right")
            return getRightIndex(i, displacement);
        
        return 0;
    }


    /**
     * \brief Return the number of point in the domain
     *
     * \return The number of point in the domain
     */
    inline std::size_t getPointCount() const
    {
        return xAxis.n;
    }

    /**
     * \brief Return the i-th point of the domain
     *
     * \param at The index of the pointgetSt
     * \param [out] x The coordinate of the point
     */
    inline void getPoint(std::size_t at, T& x) const
    {
        x = xAxis.getPoint(at);
    }

    /**
     * \brief Returns the dimension along x-Axis
     *
     * \return The dimension along the x-Axis
     */
    inline std::size_t getXDimension() const
    {
        return xAxis.n;
    }

    inline T gethx() const
    {
        return (xAxis.end - xAxis.start) / (xAxis.n - 1);
    }
protected:
    Axis<T> xAxis /**< xAxis */;
};

/**
 * \brief 2D Grid
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class Grid2D : public Mesh2D<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param xy x and y axes defining the grid
     */
    Grid2D(const Axis<T>& xy) : xAxis(xy), yAxis(xy)
    {

    }

    /**
     * \brief Ctor
     *
     * \param x axis defining the grid
     * \param y axis defining the grid
     */
    Grid2D(const Axis<T>& x, const Axis<T>& y) : xAxis(x), yAxis(y)
    {

    }

    /**
     * \brief Return the number of point in the left border
     *
     * \return The number of point in the left border
     */
    std::size_t getLeftIndicesCount() const
    {
        return yAxis.n;
    }

    /**
     * \brief Return the i-th point of the left border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the left border
     */
    std::size_t getLeftIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return i * xAxis.n + displacement;
    }

    /**
     * \brief Return the i-th point of the right border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the right border
     */
    std::size_t getRightIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return getLeftIndex(i) + (xAxis.n - 1) - displacement;
    }

    /**
     * \brief Return the number of point in the front border
     *
     * \return The number of point in the front border
     */
    std::size_t getFrontIndicesCount() const
    {
        return xAxis.n;
    }

    /**
     * \brief Return the i-th point of the back border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the back border
     */
    std::size_t getBackIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return i + displacement * xAxis.n;
    }

    /**
     * \brief Return the i-th point of the front border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the front border
     */
    std::size_t getFrontIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return getBackIndex(i) + (yAxis.n - 1) * xAxis.n - displacement * xAxis.n;
    }

    /**
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border
     */
    inline std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndicesCount();
        else if (borderName == "right")
            return getLeftIndicesCount();
        else if (borderName == "front")
            return getFrontIndicesCount();
        else if (borderName == "back")
            return getFrontIndicesCount();
        
        return 0;
    }

    /**
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */
    inline std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndex(i, displacement);
        else if (borderName == "right")
            return getRightIndex(i, displacement);
        else if (borderName == "front")
            return getFrontIndex(i, displacement);
        else if (borderName == "back")
            return getBackIndex(i, displacement);

        return 0;
    }

    inline Grid1D<T> border(const std::string& border)
    {
        if (border == "left")
            return Grid1D<T>(yAxis);
        else if (border == "right")
            return Grid1D<T>(yAxis);
        else if (border == "front")
            return Grid1D<T>(xAxis);
        else if (border == "back")
            return Grid1D<T>(xAxis);

        return Grid1D<T>(Axis<T>(0, T{}, T{}));
    }

    /**
     * \brief Return the number of point in the domain
     *
     * \return The number of point in the domain
     */
    inline std::size_t getPointCount() const
    {
        return xAxis.n * yAxis.n;
    }

    /**
     * \brief Return the i-th point of the domain
     *
     * \param i The index of the point
     * \param [out] x The x-coordinate of the point
     * \param [out] y The y-coordinate of the point
     */
    inline void getPoint(std::size_t i, T& x, T& y) const
    {
        std::size_t idx, idy;
        
        idx = i % xAxis.n;
        idy = static_cast<std::size_t>(i / xAxis.n);

        x = xAxis.getPoint(idx);
        y = yAxis.getPoint(idy);
    }

    /**
     * \brief Returns the dimension along the x-Axis
     *
     * \return The dimension along the x-Axis
     */
    inline std::size_t getXDimension() const
    {
        return xAxis.n;
    }

    inline T gethx() const
    {
        return (xAxis.end - xAxis.start) / (xAxis.n - 1);
    }


    /**
     * \brief Returns the dimension along the y-Axis
     *
     * \return The dimension along the y-Axis
     */
    inline std::size_t getYDimension() const
    {
        return yAxis.n;
    }

    inline T gethy() const
    {
        return (yAxis.end - yAxis.start) / (yAxis.n - 1);
    }

    void print() const
    {
        std::cout << "[" << xAxis.start << "," << xAxis.end << "] x [" << yAxis.start << "," << yAxis.end << "]\n";
    }

protected:
    Axis<T> xAxis;
    Axis<T> yAxis;
};

/**
 * \brief 3D Grid
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class Grid3D : public Mesh3D<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param xyz x, y and z axes defining the grid
     */
    Grid3D(const Axis<T>& xyz) : xAxis(xyz), yAxis(xyz), zAxis(xyz)
    {

    }

    /**
     * \brief Ctor
     *
     * \param x axis defining the grid
     * \param y axis defining the grid
     * \param z axis defining the grid
     */
    Grid3D(const Axis<T>& x, const Axis<T>& y, const Axis<T>& z) : xAxis(x), yAxis(y), zAxis(z)
    {

    }

    /**
     * \brief Return the number of point in the left border
     *
     * \return The number of point in the left border
     */
    std::size_t getLeftIndicesCount() const
    {
        return zAxis.n * yAxis.n;
    }

    /**
     * \brief Return the i-th point of the left border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the left border
     */
    std::size_t getLeftIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return i * xAxis.n + displacement;
    }

    /**
     * \brief Return the i-th point of the right border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the right border
     */
    std::size_t getRightIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return getLeftIndex(i) + (xAxis.n - 1) - displacement;
    }

    /**
     * \brief Return the number of point in the front border
     *
     * \return The number of point in the front border
     */
    std::size_t getFrontIndicesCount() const
    {
        return xAxis.n * zAxis.n;
    }

    /**
    * \brief Return the i-th point of the back border
    *
    * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
    *
    * \return The i-th point of the back border
    */
    std::size_t getBackIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return (i % xAxis.n) + static_cast<std::size_t>(i / xAxis.n) * xAxis.n * yAxis.n + displacement * xAxis.n;
    }

    /**
     * \brief Return the i-th point of the front border
     *
     * \param i The index of the point
     * \param displacement The inner border index. 0 is the border of the domain,
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of the front border
     */
    std::size_t getFrontIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return getBackIndex(i) + (yAxis.n - 1) * xAxis.n - displacement * xAxis.n;
    }

    /**
     * \brief Return the number of point in the top border
     *
     * \return The number of point in the top border
     */
    std::size_t getTopIndicesCount() const
    {
        return xAxis.n * yAxis.n;
    }

    /**
    * \brief Return the i-th point of the bottom border
    *
    * \param i The index of the point
    * \param displacement The inner border index. 0 is the border of the domain,
    *     1 is the border of the domain without the border, ...(default is 0)
    *
    * \return The i-th point of the bottom border
    */
    std::size_t getBotIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return i + displacement * xAxis.n * yAxis.n;
    }

    /**
    * \brief Return the i-th point of the top border
    *
    * \param i The index of the point
    * \param displacement The inner border index. 0 is the border of the domain, 
    *     1 is the border of the domain without the border, ...(default is 0)
    *
    * \return The i-th point of the top border
    */
    std::size_t getTopIndex(std::size_t i, std::size_t displacement = 0) const
    {
        return getBotIndex(i) + xAxis.n * yAxis.n * (zAxis.n - 1) - displacement * xAxis.n * yAxis.n;
    }

    /**
     * \brief Return the number of point in a border
     *
     * \param borderName The name of the border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The number of point in a border
     */
    inline std::size_t getBorderPointCount(const std::string& borderName, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndicesCount();
        else if (borderName == "right")
            return getLeftIndicesCount();
        else if (borderName == "front")
            return getFrontIndicesCount();
        else if (borderName == "back")
            return getFrontIndicesCount();
        else if (borderName == "top")
            return getTopIndicesCount();
        else if (borderName == "bot")
            return getTopIndicesCount();

        std::cout << "Unknown border" << std::endl;

        return 0;
    }

    /**
     * \brief Return the i-th point of the border
     *
     * \param borderName The name of a border
     * \param displacement The inner border index. 0 is the border of the domain, 
     *     1 is the border of the domain without the border, ...(default is 0)
     *
     * \return The i-th point of a border
     */ 
    inline std::size_t getBorderPoint(const std::string& borderName, std::size_t i, std::size_t displacement = 0) const
    {
        if (borderName == "left")
            return getLeftIndex(i, displacement);
        else if (borderName == "right")
            return getRightIndex(i, displacement);
        else if (borderName == "front")
            return getFrontIndex(i, displacement);
        else if (borderName == "back")
            return getBackIndex(i, displacement);
        else if (borderName == "top")
            return getTopIndex(i, displacement);
        else if (borderName == "bot")
            return getBotIndex(i, displacement);

        return 0;
    }

    inline Grid2D<T> border(const std::string& border)
    {
        if (border == "left")
            return Grid2D<T>(yAxis, zAxis);
        else if (border == "right")
            return Grid2D<T>(yAxis, zAxis);
        else if (border == "front")
            return Grid2D<T>(xAxis, zAxis);
        else if (border == "back")
            return Grid2D<T>(xAxis, zAxis);
        else if (border == "top")
            return Grid2D<T>(xAxis, yAxis);
        else if (border == "bot")
            return Grid2D<T>(xAxis, yAxis);

        return Grid2D<T>(Axis<T>(0, T{}, T{}));
    }

    /**
     * \brief Return the number of point in the domain
     *
     * \return The number of point in the domain
     */
    inline std::size_t getPointCount() const
    {
        return xAxis.n * yAxis.n * zAxis.n;
    }

    /**
     * \brief Return the i-th point of the domain
     *
     * \param i The index of the point
     * \param [out] x The x-coordinate of the point
     * \param [out] y The y-coordinate of the point
     * \param [out] z The z-coordinate of the point
     */
    inline void getPoint(std::size_t i, T& x, T& y, T& z) const
    {
        std::size_t idx, idy, idz;
        idz = static_cast<std::size_t>(i / (xAxis.n * yAxis.n));
        idy = static_cast<std::size_t>((i - idz * (xAxis.n * yAxis.n)) / xAxis.n);
        idx = (i - idz * (xAxis.n * yAxis.n)) % xAxis.n;

        x = xAxis.getPoint(idx);
        y = yAxis.getPoint(idy);
        z = zAxis.getPoint(idz);
    }

    /**
     * \brief Returns the dimension along the x-Axis
     *
     * \return The dimension along the x-Axis
     */
    inline std::size_t getXDimension() const
    {
        return xAxis.n;
    }

    inline T gethx() const
    {
        return (xAxis.end - xAxis.start) / (xAxis.n - 1);
    }

    /**
     * \brief Returns the dimension along the y-Axis
     *
     * \return The dimension along the y-Axis
     */
    inline std::size_t getYDimension() const
    {
        return yAxis.n;
    }

    inline T gethy() const
    {
        return (yAxis.end - yAxis.start) / (yAxis.n - 1);
    }
    
    /**
     * \brief Returns the dimension along the z-Axis
     *
     * \return The dimension along the z-Axis
     */
    inline std::size_t getZDimension() const
    {
        return zAxis.n;
    }

    inline T gethz() const
    {
        return (zAxis.end - zAxis.start) / (zAxis.n - 1);
    }


    void print() const
    {
        std::cout << "[" << xAxis.start << "," << xAxis.end << "] x [" << yAxis.start << "," << yAxis.end << "]" << "] x [" << zAxis.start << "," << zAxis.end << "]\n";
    }
protected:
    Axis<T> xAxis;
    Axis<T> yAxis;
    Axis<T> zAxis;
};