#pragma once

#include <chrono>
#include <vector>
#include <iostream>

#include "../MPIBinding/utils.h"

#include "mpi.h"

#define TIME(name, x) \
        auto start_##name = std::chrono::steady_clock::now();\
        x;\
        auto end_##name = std::chrono::steady_clock::now();\
        std::chrono::duration<double> ##name = end_##name - start_##name;

struct Benchmark
{
public:
    std::vector<double> commTimes;
    std::vector<double> gcTimes;
    std::vector<double> itTimes;
    double totalTime = 0.0;
    double setupTime = 0.0;
};

struct AggregatedBenchmark
{
    double mean_total_time = 0.0;
    double mean_comm_time  = 0.0;
    double mean_gc_time    = 0.0;
    double mean_setup      = 0.0;

    void print() const
    {
        std::cout << "Mean total time : " << mean_total_time << std::endl;
        std::cout << "Mean setup time : " << mean_setup << std::endl;
        std::cout << "Mean gc    time : " << mean_gc_time << std::endl;
        std::cout << "Mean comm  time : " << mean_comm_time << std::endl;
    }
};

void ReduceBenchmark(const Benchmark& local, MPI_Comm comm, int to = 0)
{
    AggregatedBenchmark agg;

    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    double local_gc = 0.0;
    for (std::size_t i = 1; i < local.gcTimes.size(); i++)   local_gc   += local.gcTimes[i];

    double local_comm = 0.0;
    for (std::size_t i = 1; i < local.commTimes.size(); i++) local_comm += local.commTimes[i];

    double count = local.gcTimes.size() - 1;

    // double totalIt     = Reduce(count, comm);
    double total_comm  = Reduce(local_comm, comm);
    double total_gc    = Reduce(local_gc, comm);
    double total_time  = Reduce(local.totalTime, comm);
    double total_setup = Reduce(local.setupTime, comm);

    if (rank == 0)
    {
        agg.mean_total_time = total_time / size;
        agg.mean_gc_time   = total_gc    / size;
        agg.mean_comm_time = total_comm  / size;
        agg.mean_setup = total_setup     / size;
    
        agg.print();
    }
}