#pragma once

#include "Benchmark_utils.h"

#include "../PDE/Laplacian.h"
#include "../PDE/DirichletConditionsGrid.h"
#include "../MPIBinding/DistributedMesh/DistributedCartesianMesh.h"
#include "../MPIBinding/VectorExchanger/SendReceiveVectorExchanger.h"
#include "../MPIBinding/VectorExchanger/RMAVectorExchanger.h"

double f(double x, double y)
{
    return x * (x - 1.0) + y * (y - 1.0);
}

double y_exact(double x, double y)
{
    return 0.5 * x * (x - 1.0) * y * (y - 1.0);
}

void benchmark_lp2d(std::size_t n, std::size_t history_size)
{
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

        DistributedGrid2D<double> grid(
            Axis<double>(n, hx, 1 - hx),
            Axis<double>(n, hx, 1 - hx)
        );

        SendReceiveVectorExchanger<double> left(&grid,
            grid.getBorderPointCount("left"),
            grid.getNeighbourMeshRank("left"),
            grid.getNeighbourMeshRank("right")
        );
        SendReceiveVectorExchanger<double> right(&grid,
            grid.getBorderPointCount("right"),
            grid.getNeighbourMeshRank("right"),
            grid.getNeighbourMeshRank("left")
        );
        SendReceiveVectorExchanger<double> front(&grid,
            grid.getBorderPointCount("front"),
            grid.getNeighbourMeshRank("front"),
            grid.getNeighbourMeshRank("back")
        );
        SendReceiveVectorExchanger<double> back(&grid,
            grid.getBorderPointCount("back"),
            grid.getNeighbourMeshRank("back"),
            grid.getNeighbourMeshRank("front")
        );

        GC_History<double> history(grid.getPointCount(), history_size);

        SDMatrix<double> lp = getLaplacian2D<double>(&grid);

        DirichletConditionsGrid2D<double> conditions(&grid);
        conditions.applyOnLinearOperator(lp);

        const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());
        const double ihy2 = 1.0 / (grid.gethy() * grid.gethy());

        Vector<double> b_f = grid.apply(f);
        Vector<double> x(grid.getPointCount());

        Vector<double> exact = grid.apply(y_exact);
        Vector<double> error_v = exact - x;
        double error = scal(error_v, error_v);
    );
    bench.setupTime = setup.count();

    TIME(it,
        for (std::size_t i = 0; i < 1000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (grid.isInLeftGlobalBoder())
                conditions.setCondition("left", Vector<double>(grid.getBorderPointCount("left")), -ihx2);
            else
                conditions.setCondition("left", right.getData(), -ihx2);

            if (grid.isInRightGlobalBoder())
                conditions.setCondition("right", Vector<double>(grid.getBorderPointCount("right")), -ihx2);
            else
                conditions.setCondition("right", left.getData(), -ihx2);

            if (grid.isInFrontGlobalBorder())
                conditions.setCondition("front", Vector<double>(grid.getBorderPointCount("front")), -ihy2);
            else
                conditions.setCondition("front", back.getData(), -ihy2);

            if (grid.isInBackGlobalBorder())
                conditions.setCondition("back", Vector<double>(grid.getBorderPointCount("back")), -ihy2);
            else
                conditions.setCondition("right", front.getData(), -ihy2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-10, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left" , x, left.refData());
            grid.gatherBorder("right", x, right.refData());
            grid.gatherBorder("front", x, front.refData());
            grid.gatherBorder("back" , x, back.refData());

            TIME(comm,
                left.exchange();
                right.exchange();
                front.exchange();
                back.exchange();
            );

            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    ReduceBenchmark(bench, grid.getComm());
}

void benchmark_lp2drmaput(std::size_t n, std::size_t history_size)
{
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

        DistributedGrid2D<double> grid(
            Axis<double>(n, hx, 1 - hx),
            Axis<double>(n, hx, 1 - hx)
        );

        RMAPutVectorsExchanger exchanger(&grid,
            {
                {"left" , { grid.getBorderPointCount("left") , { "right", grid.getNeighbourMeshRank("left")  } } },
                {"right", { grid.getBorderPointCount("right"), { "left" , grid.getNeighbourMeshRank("right") } } },
                {"front", { grid.getBorderPointCount("front"), { "back" , grid.getNeighbourMeshRank("front") } } },
                {"back" , { grid.getBorderPointCount("back") , { "front", grid.getNeighbourMeshRank("back")  } } }
            });
        exchanger.sync();


        GC_History<double> history(grid.getPointCount(), history_size);

        SDMatrix<double> lp = getLaplacian2D<double>(&grid);

        DirichletConditionsGrid2D<double> conditions(&grid);
        conditions.applyOnLinearOperator(lp);

        const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());

        Vector<double> b_f = grid.apply(f);
        Vector<double> x(grid.getPointCount());

        Vector<double> exact = grid.apply(y_exact);
        Vector<double> error_v = exact - x;
        double error = scal(error_v, error_v);
        );
    bench.setupTime = setup.count();

    TIME(it,
        for (std::size_t i = 0; i < 1000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (!grid.isInLeftGlobalBoder())
                conditions.setCondition("left", exchanger.getData("left"), -ihx2);

            if (!grid.isInRightGlobalBoder())
                conditions.setCondition("right", exchanger.getData("right"), -ihx2);

            if (!grid.isInFrontGlobalBorder())
                conditions.setCondition("front", exchanger.getData("front"), -ihx2);

            if (!grid.isInBackGlobalBorder())
                conditions.setCondition("back", exchanger.getData("back"), -ihx2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-5, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left" , x, exchanger.refData("left"));
            grid.gatherBorder("right", x, exchanger.refData("right"));
            grid.gatherBorder("front", x, exchanger.refData("front"));
            grid.gatherBorder("back" , x, exchanger.refData("back"));

            TIME(comm,
                exchanger.exchangeall();
                exchanger.sync();
            );
            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    ReduceBenchmark(bench, grid.getComm());
}

void benchmark_lp2drmaget(std::size_t n, std::size_t history_size)
{
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

    DistributedGrid2D<double> grid(
        Axis<double>(n, hx, 1 - hx),
        Axis<double>(n, hx, 1 - hx)
    );

    RMAGetVectorsExchanger exchanger(&grid,
        {
            {"left" , { grid.getBorderPointCount("left") , { "right", grid.getNeighbourMeshRank("left")  } } },
            {"right", { grid.getBorderPointCount("right"), { "left" , grid.getNeighbourMeshRank("right") } } },
            {"front", { grid.getBorderPointCount("front"), { "back" , grid.getNeighbourMeshRank("front") } } },
            {"back" , { grid.getBorderPointCount("back") , { "front", grid.getNeighbourMeshRank("back")  } } }
        });
    exchanger.sync();


    GC_History<double> history(grid.getPointCount(), history_size);

    SDMatrix<double> lp = getLaplacian2D<double>(&grid);

    DirichletConditionsGrid2D<double> conditions(&grid);
    conditions.applyOnLinearOperator(lp);

    const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());

    Vector<double> b_f = grid.apply(f);
    Vector<double> x(grid.getPointCount());

    Vector<double> exact = grid.apply(y_exact);
    Vector<double> error_v = exact - x;
    double error = scal(error_v, error_v);
    );
    bench.setupTime = setup.count();

    TIME(it,
        for (std::size_t i = 0; i < 1000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (grid.isInLeftGlobalBoder())
                conditions.setCondition("left", exchanger.getData("left"), -ihx2);

            if (!grid.isInRightGlobalBoder())
                conditions.setCondition("right", exchanger.getData("right"), -ihx2);

            if (!grid.isInFrontGlobalBorder())
                conditions.setCondition("front", exchanger.getData("front"), -ihx2);

            if (!grid.isInBackGlobalBorder())
                conditions.setCondition("back", exchanger.getData("back"), -ihx2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-5, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left", x, exchanger.refData("left"));
            grid.gatherBorder("right", x, exchanger.refData("right"));
            grid.gatherBorder("front", x, exchanger.refData("front"));
            grid.gatherBorder("back", x, exchanger.refData("back"));

            TIME(comm,
                exchanger.exchangeall();
            exchanger.sync();
            );
            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    ReduceBenchmark(bench, grid.getComm());
}