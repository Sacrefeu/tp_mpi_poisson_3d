#pragma once

#include "Benchmark_utils.h"

#include "../PDE/Laplacian.h"
#include "../PDE/DirichletConditionsGrid.h"
#include "../MPIBinding/DistributedMesh/DistributedCartesianMesh.h"
#include "../MPIBinding/VectorExchanger/SendReceiveVectorExchanger.h"
#include "../MPIBinding/VectorExchanger/RMAVectorExchanger.h"

double f(double x)
{
    return 1.0;
}

double y_exact(double x)
{
    return 0.5 * x * (x - 1);
}

void benchmark_lp1d(std::size_t n, std::size_t history_size)
{
    Vector<double> error_right(2000);
    Vector<double> error_left(2000);
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

        DistributedGrid1D<double> grid(Axis<double>(n, hx, 1 - hx));
        SendReceiveVectorExchanger<double> left(&grid,
            grid.getBorderPointCount("left"),
            grid.getNeighbourMeshRank("left"),
            grid.getNeighbourMeshRank("right")
        );
        SendReceiveVectorExchanger<double> right(&grid,
            grid.getBorderPointCount("right"),
            grid.getNeighbourMeshRank("right"),
            grid.getNeighbourMeshRank("left")
        );

        GC_History<double> history(grid.getPointCount(), history_size);

        SDMatrix<double> lp = getLaplacian1D<double>(&grid);

        DirichletConditionsGrid1D<double> conditions(&grid);
        conditions.applyOnLinearOperator(lp);

        const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());

        Vector<double> b_f = grid.apply(f);
        Vector<double> x(grid.getPointCount());

        Vector<double> exact = grid.apply(y_exact);
        Vector<double> error_v = exact - x;
        double error = scal(error_v, error_v);
    );
    bench.setupTime = setup.count();

    TIME(it,
        for(std::size_t i = 0; i < 2000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (grid.isInLeftGlobalBoder())
                conditions.setCondition("left", { 0.0 }, -ihx2);
            else
                conditions.setCondition("left", right.getData(), -ihx2);

            if (grid.isInRightGlobalBoder())
                conditions.setCondition("right", { 0.0 }, -ihx2);
            else
                conditions.setCondition("right", left.getData(), -ihx2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-5, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left" , x, left.refData());
            grid.gatherBorder("right", x, right.refData());

            TIME(comm,
                left.exchange();
                right.exchange();
            );
            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);

            grid.gatherBorder("left" , error_v, error_left.at(i));
            grid.gatherBorder("right", error_v, error_right.at(i));
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    toCSVFile("rslt/error_border_left_"  + std::to_string(grid.getRank()) + ".csv", error_left);
    toCSVFile("rslt/error_border_right_" + std::to_string(grid.getRank()) + ".csv", error_right);
    ReduceBenchmark(bench, grid.getComm());
}

void benchmark_lp1drmaput(std::size_t n, std::size_t history_size)
{
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

        DistributedGrid1D<double> grid(Axis<double>(n, hx, 1 - hx));

        RMAPutVectorsExchanger exchanger(&grid,
            {
                {"left" , { 2, { "right", grid.getNeighbourMeshRank("left")  } } },
                {"right", { 2, { "left" , grid.getNeighbourMeshRank("right") } } }
            });
        exchanger.sync();


        GC_History<double> history(grid.getPointCount(), history_size);

        SDMatrix<double> lp = getLaplacian1D<double>(&grid);

        DirichletConditionsGrid1D<double> conditions(&grid);
        conditions.applyOnLinearOperator(lp);

        const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());

        Vector<double> b_f = grid.apply(f);
        Vector<double> x(grid.getPointCount());

        Vector<double> exact = grid.apply(y_exact);
        Vector<double> error_v = exact - x;
        double error = scal(error_v, error_v);
    );
    bench.setupTime = setup.count();

    TIME(it,
        for (std::size_t i = 0; i < 1000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (grid.isInLeftGlobalBoder())
                conditions.setCondition("left", { 0.0 }, -ihx2); 
            else
                conditions.setCondition("left", exchanger.getData("left"), -ihx2);

            if (grid.isInRightGlobalBoder())
                conditions.setCondition("right", { 0.0 }, -ihx2);
            else
                conditions.setCondition("right", exchanger.getData("right"), -ihx2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-5, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left" , x, exchanger.refData("left"));
            grid.gatherBorder("right", x, exchanger.refData("right"));

            TIME(comm,
                exchanger.exchangeall();
                exchanger.sync();
            );
            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    ReduceBenchmark(bench, grid.getComm());
}

void benchmark_lp1drmaget(std::size_t n, std::size_t history_size)
{
    Benchmark bench;

    TIME(setup,
        double hx = 1.0 / (n - 1);

    DistributedGrid1D<double> grid(Axis<double>(n, hx, 1 - hx));

    RMAGetVectorsExchanger exchanger(&grid,
        {
            {"left" , { 2, { "right", grid.getNeighbourMeshRank("left")  } } },
            {"right", { 2, { "left" , grid.getNeighbourMeshRank("right") } } }
        });


    GC_History<double> history(grid.getPointCount(), history_size);

    SDMatrix<double> lp = getLaplacian1D<double>(&grid);

    DirichletConditionsGrid1D<double> conditions(&grid);
    conditions.applyOnLinearOperator(lp);

    const double ihx2 = 1.0 / (grid.gethx() * grid.gethx());

    Vector<double> b_f = grid.apply(f);
    Vector<double> x(grid.getPointCount());

    Vector<double> exact = grid.apply(y_exact);
    Vector<double> error_v = exact - x;
    double error = scal(error_v, error_v);
    );
    bench.setupTime = setup.count();

    TIME(it,
        for (std::size_t i = 0; i < 1000; i++)
        {
            Vector<double> b = b_f;
            conditions.reset();

            if (grid.isInLeftGlobalBoder())
                conditions.setCondition("left", { 0.0 }, -ihx2);
            else
                conditions.setCondition("left", exchanger.getData("left"), -ihx2);

            if (grid.isInRightGlobalBoder())
                conditions.setCondition("right", { 0.0 }, -ihx2);
            else
                conditions.setCondition("right", exchanger.getData("right"), -ihx2);

            conditions.applyOnRhs(b);

            TIME(gc,
                GC(history, lp, b, x, 1e-5, 100000);
            );
            bench.gcTimes.push_back(gc.count());

            grid.gatherBorder("left", x, exchanger.refData("left"));
            grid.gatherBorder("right", x, exchanger.refData("right"));

            TIME(comm,
                exchanger.sync();
            exchanger.exchangeall();
            exchanger.sync();
            );
            bench.commTimes.push_back(comm.count());

            error_v = exact - x;
            error = scal(error_v, error_v);
        }
    );
    bench.totalTime = it.count() + bench.setupTime;

    ReduceBenchmark(bench, grid.getComm());
}