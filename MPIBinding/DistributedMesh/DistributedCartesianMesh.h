#pragma once

#include "../../Mesh/CartesianMesh.h"
#include "../Topology/CartesianTopology.h"

template<typename T>
Axis<T> setUpAxis(int dim, int coord, bool start, bool end, int displacement, const Axis<T>& axis)
{
    Axis<T> new_axis(0, T{}, T{});

    new_axis.n = axis.n / dim + (!start + !end) * displacement;

    new_axis.start = axis.start + (coord + 0) * (axis.end - axis.start) / dim - (!start) * (double)displacement / new_axis.n;
    new_axis.end   = axis.start + (coord + 1) * (axis.end - axis.start) / dim + (!end)   * (double)displacement / new_axis.n;

    return new_axis;
}

template<typename T>
class DistributedGrid1D : public Grid1D<T>, public CartesianTopology
{
public:
    DistributedGrid1D(const Axis<T>& axis, std::size_t displacement = 0, MPI_Comm c = MPI_COMM_WORLD) :
        CartesianTopology(1, c),
        Grid1D<T>(axis)
    {
        isLeftBorder  = (coords[0] == 0);
        isRightBorder = (coords[0] == dims[0] - 1);

        this->xAxis = setUpAxis(dims[0], coords[0], isLeftBorder, isRightBorder, displacement, axis);
    }

    void print()
    {
        std::cout << "[" << rank << "] : [" << this->xAxis.start << " , " << this->xAxis.end << " ] (" << this->xAxis.n << ")\n";
    }

    bool isInLeftGlobalBoder() const
    {
        return isLeftBorder;
    }

    bool isInRightGlobalBoder() const
    {
        return isRightBorder;
    }

    int getNeighbourMeshRank(const std::string& dir) const
    {
        int neighbourRank;
        int tmpRank = rank;

        if (dir == "left")
            MPI_Cart_shift(comm, 0, -1, &tmpRank, &neighbourRank);
        else if (dir == "right")
            MPI_Cart_shift(comm, 0,  1, &tmpRank, &neighbourRank);
        else
            neighbourRank = tmpRank;

        return neighbourRank;
    }
private:
    bool isLeftBorder;
    bool isRightBorder;
};

template<typename T>
class DistributedGrid2D : public Grid2D<T>, public CartesianTopology
{
public:
    DistributedGrid2D(const Axis<T>& xAxis, const Axis<T>& yAxis, std::size_t displacement = 0, MPI_Comm c = MPI_COMM_WORLD) :
        CartesianTopology(2, c),
        Grid2D<T>(xAxis, yAxis)
    {
        isLeftBorder  = (coords[0] == 0);
        isRightBorder = (coords[0] == dims[0] - 1);
        isFrontBorder = (coords[1] == 0);
        isBackBorder  = (coords[1] == dims[1] - 1);

        this->xAxis = setUpAxis(dims[0], coords[0], isLeftBorder, isRightBorder, displacement, xAxis);
        this->yAxis = setUpAxis(dims[1], coords[1], isFrontBorder, isBackBorder, displacement, yAxis);
    }

    void print()
    {
        std::cout << "[" << rank << "] : (" << coords[0] << "," << coords[1] << ")" << std::endl;
        std::cout << "[" << rank << "] : [" << this->xAxis.start << "," << this->xAxis.end << "] x [" << this->yAxis.start << "," << this->yAxis.end << "]" << std::endl;
    }

    bool isInLeftGlobalBoder() const
    {
        return isLeftBorder;
    }

    bool isInRightGlobalBoder() const
    {
        return isRightBorder;
    }

    bool isInFrontGlobalBorder() const
    {
        return isFrontBorder;
    }

    bool isInBackGlobalBorder() const
    {
        return isBackBorder;
    }

    int getNeighbourMeshRank(const std::string& dir) const
    {
        int neighbourRank;
        int tmpRank = rank;

        if (dir == "left")
            MPI_Cart_shift(comm, 0, -1, &tmpRank, &neighbourRank);
        else if (dir == "right")
            MPI_Cart_shift(comm, 0, 1, &tmpRank, &neighbourRank);
        else if (dir == "front")
            MPI_Cart_shift(comm, 1, 1, &tmpRank, &neighbourRank);
        else if (dir == "back")
            MPI_Cart_shift(comm, 1, -1, &tmpRank, &neighbourRank);
        else
            neighbourRank = tmpRank;

        return neighbourRank;
    }
private:
    bool isLeftBorder;
    bool isRightBorder;
    bool isFrontBorder;
    bool isBackBorder;
};

template<typename T>
class DistributedGrid3D : public Grid3D<T>, public CartesianTopology
{
public:
    DistributedGrid3D(const Axis<T>& xAxis, const Axis<T>& yAxis, const Axis<T>& zAxis, std::size_t displacement = 0, MPI_Comm c = MPI_COMM_WORLD) :
        CartesianTopology(3, c),
        Grid3D<T>(xAxis, yAxis, zAxis)
    {
        isLeftBorder  = (coords[0] == 0);
        isRightBorder = (coords[0] == dims[0] - 1);
        isFrontBorder = (coords[1] == 0);
        isBackBorder  = (coords[1] == dims[1] - 1);
        isTopBorder   = (coords[2] == 0);
        isBotBorder   = (coords[2] == dims[2] - 1);

        this->xAxis = setUpAxis(dims[0], coords[0], isLeftBorder, isRightBorder, displacement, xAxis);
        this->yAxis = setUpAxis(dims[1], coords[1], isFrontBorder, isBackBorder, displacement, yAxis);
        this->zAxis = setUpAxis(dims[2], coords[2], isTopBorder, isBotBorder   , displacement, zAxis);
    }

    void print()
    {
        std::cout << "Process " << rank << "(" << coords[0] << "," << coords[1] << "," << coords[2] << ")" << std::endl;
    }

    bool isInLeftGlobalBoder() const
    {
        return isLeftBorder;
    }

    bool isInRightGlobalBoder() const
    {
        return isRightBorder;
    }

    bool isInFrontGlobalBorder() const
    {
        return isFrontBorder;
    }

    bool isInBackGlobalBorder() const
    {
        return isBackBorder;
    }

    bool isInTopGlobalBorder() const
    {
        return isTopBorder;
    }

    bool isInBotGlobalBorder() const
    {
        return isBotBorder;
    }

    int getNeighbourMeshRank(const std::string& dir) const
    {
        int neighbourRank;
        int tmpRank = rank;

        if (dir == "left")
            MPI_Cart_shift(comm, 0, -1, &tmpRank, &neighbourRank);
        else if (dir == "right")
            MPI_Cart_shift(comm, 0, 1, &tmpRank, &neighbourRank);
        else if (dir == "front")
            MPI_Cart_shift(comm, 1, 1, &tmpRank, &neighbourRank);
        else if (dir == "back")
            MPI_Cart_shift(comm, 1, -1, &tmpRank, &neighbourRank);
        else if (dir == "top")
            MPI_Cart_shift(comm, 2, 1, &tmpRank, &neighbourRank);
        else if (dir == "bot")
            MPI_Cart_shift(comm, 2, -1, &tmpRank, &neighbourRank);
        else
            neighbourRank = tmpRank;

        return neighbourRank;
    }
private:
    bool isLeftBorder;
    bool isRightBorder;
    bool isFrontBorder;
    bool isBackBorder;
    bool isTopBorder;
    bool isBotBorder;
};