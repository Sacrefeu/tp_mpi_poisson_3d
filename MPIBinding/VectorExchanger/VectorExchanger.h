#pragma once

#include "../../Linalg/linalg.h"
#include "../Topology/Topology.h"

template<typename T>
class VectorExchanger
{
public:
    VectorExchanger(Topology* topology, int dim, int dest, int from) :
        toSend(dim, T{}),
        toReceive(dim, T{})
    {
        rank = topology->getRank();
        comm = topology->getComm();
        sendRank = dest;
        receiveRank = from;
    }

    virtual void exchange() = 0;
    
    Vector<T>& refData()
    {
        return toSend;
    }

    void setData(const Vector<T>& send)
    {
        toSend = send; 
    }

    Vector<T> getData() const
    {
        return toReceive;
    }
protected:
    Vector<T> toSend;
    Vector<T> toReceive;

    int rank;
    int sendRank;
    int receiveRank;
    MPI_Comm comm;
};