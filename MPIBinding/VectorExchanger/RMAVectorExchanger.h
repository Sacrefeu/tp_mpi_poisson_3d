#pragma once

#include <map>

#include "VectorExchanger.h"
#include "mpi.h"

class NaiveRMAPutVectorExchanger : public VectorExchanger<double>
{
public:
    NaiveRMAPutVectorExchanger(Topology* topology, int dim, int dest, int from) :
        VectorExchanger<double>(topology, dim, dest, from)
    {
        MPI_Win_create(&this->toReceive[0], this->toReceive.size() * sizeof(double), sizeof(double), MPI_INFO_NULL, topology->getComm(), &send_window);
    }

    virtual void exchange()
    {
        MPI_Put(&this->toSend[0], this->toSend.size(), MPI_DOUBLE, this->sendRank, 0, this->toSend.size(), MPI_DOUBLE, send_window);
    }

    void sync() const
    {
        MPI_Win_fence(0, send_window);
    }
private:
    MPI_Win    send_window;
};


class NaiveRMAGetVectorExchanger : public VectorExchanger<double>
{
public:
    NaiveRMAGetVectorExchanger(Topology* topology, int dim, int dest, int from) :
        VectorExchanger<double>(topology, dim, dest, from)
    {
        MPI_Win_create(&this->toSend[0], this->toSend.size() * sizeof(double), sizeof(double), MPI_INFO_NULL, topology->getComm(), &receive_window);
    }

    virtual void exchange()
    {
        MPI_Get(&this->toReceive[0], this->toReceive.size(), MPI_DOUBLE, this->receiveRank, 0, this->toReceive.size(), MPI_DOUBLE, receive_window);
    }

    void sync() const
    {
        MPI_Win_fence(0, receive_window);
    }
private:
    MPI_Win receive_window;
};



class RMAVectorsExchanger
{
public:
    struct RMAInfo
    {
        std::size_t offset;
        std::size_t size;

        std::string nameDest;
        int dest;
    };

    RMAVectorsExchanger(Topology* topology, const std::map<std::string, std::pair<std::size_t, std::pair<std::string, int>>>& vectors)
    {
        this->topology = topology;

        std::size_t total_size = 0;
        for (const auto& v : vectors)
        {
            // { name,  { size, {name, dest }}}
            vectorsInfos[v.first] = { total_size, v.second.first, v.second.second.first, v.second.second.second };
            total_size += v.second.first;
        }

        send = Vector<double>(total_size);
        data = Vector<double>(total_size);
    }
    
    void setData(const std::string& dataName, const Vector<double>& data)
    {
        if (vectorsInfos.find(dataName) == vectorsInfos.end()) return;

        const std::size_t start = vectorsInfos[dataName].offset;
        const std::size_t end   = vectorsInfos[dataName].offset + vectorsInfos[dataName].size;

        if (data.dim() != vectorsInfos[dataName].size) return;

        for (std::size_t i = start; i < end; i++)
        {
            send[i] = data[i - start];
        }
    }

    double* refData(const std::string& dataName)
    {
        const std::size_t start = vectorsInfos[dataName].offset;
        return &send[start];
    }

    const double* getData(const std::string& dataName) const
    {
        const std::size_t start = vectorsInfos.at(dataName).offset;
        return data.at(start);
    }

    virtual void exchange(const std::string& dataName) = 0;
    
    virtual void exchangeall()
    {
        for (const auto& vinfo : vectorsInfos)
            exchange(vinfo.first);
    }
    
    virtual void sync() const
    {
        MPI_Win_fence(0, win);
    }
protected:
    Topology* topology;
    Vector<double> send;
    Vector<double> data;

    MPI_Win win;
    std::map<std::string, RMAInfo> vectorsInfos;
};

class RMAPutVectorsExchanger : public RMAVectorsExchanger
{
public:
    RMAPutVectorsExchanger(Topology* topology, const std::map<std::string, std::pair<std::size_t, std::pair<std::string, int>>>& vectors) :
        RMAVectorsExchanger(topology, vectors)
    {
        MPI_Win_create(&this->data[0], this->data.size() * sizeof(double), sizeof(double), MPI_INFO_NULL, topology->getComm(), &this->win);
    }

    virtual void exchange(const std::string& dataName)
    {
        const std::size_t start_send = vectorsInfos.at(dataName).offset;
        const std::size_t  size_send = vectorsInfos.at(dataName).size;

        const std::size_t start_with = vectorsInfos.at(vectorsInfos.at(dataName).nameDest).offset;
        const std::size_t  size_with = vectorsInfos.at(vectorsInfos.at(dataName).nameDest).size;

        const int rank_with = vectorsInfos.at(dataName).dest;

        if (size_send != size_with) return;
       // std::cout << "Putting : " << dataName << " to " << vectorsInfos.at(dataName).nameDest << std::endl;
        MPI_Put(this->send.at(start_send), size_send, MPI_DOUBLE, rank_with, start_with, size_with, MPI_DOUBLE, win);
    }  
private:
};


class RMAGetVectorsExchanger : public RMAVectorsExchanger
{
public:
    RMAGetVectorsExchanger(Topology* topology, const std::map<std::string, std::pair<std::size_t, std::pair<std::string, int>>>& vectors) :
        RMAVectorsExchanger(topology, vectors)
    {
        MPI_Win_create(&this->send[0], this->send.size() * sizeof(double), sizeof(double), MPI_INFO_NULL, topology->getComm(), &this->win);
    }

    virtual void exchange(const std::string& dataName)
    {
        const std::size_t start_send = vectorsInfos.at(dataName).offset;
        const std::size_t  size_send = vectorsInfos.at(dataName).size;

        const std::size_t start_with = vectorsInfos.at(vectorsInfos.at(dataName).nameDest).offset;
        const std::size_t  size_with = vectorsInfos.at(vectorsInfos.at(dataName).nameDest).size;

        const int rank_with = vectorsInfos.at(dataName).dest;

        if (size_send != size_with) return; 

        MPI_Get(this->data.at(start_send), size_send, MPI_DOUBLE, rank_with, start_with, size_with, MPI_DOUBLE, win);
    }
private:
};