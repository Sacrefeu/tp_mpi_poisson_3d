#pragma once

#include "VectorExchanger.h"

template<typename T>
class SendReceiveVectorExchanger : public VectorExchanger<T>
{
public:
    SendReceiveVectorExchanger(Topology* topology, int dim, int dest, int from) : 
        VectorExchanger<T>(topology, dim, dest, from)
    {
    }

    virtual void exchange() = 0;
private:
};

template<>
class SendReceiveVectorExchanger<double> : public VectorExchanger<double>
{
public:
    SendReceiveVectorExchanger(Topology* topology, int dim, int dest, int from) :
        VectorExchanger<double>(topology, dim, dest, from)
    {
    }

    void exchange()
    {
        if (sendRank == MPI_PROC_NULL)
        {
            MPI_Recv(
                &toReceive[0], toReceive.size(), MPI_DOUBLE, receiveRank, MPI_ANY_TAG,
                comm, MPI_STATUS_IGNORE
            );
        }
        else if (receiveRank == MPI_PROC_NULL)
        {
            MPI_Send(
                &toSend[0], toSend.size(), MPI_DOUBLE, sendRank, rank, comm
            );
        }
        else
        {
            MPI_Sendrecv(           
                &toSend[0]   , toSend.size()   , MPI_DOUBLE, sendRank, rank,
                &toReceive[0], toReceive.size(), MPI_DOUBLE, receiveRank, MPI_ANY_TAG,
                comm, MPI_STATUS_IGNORE
            );
        }
    }
private:
};