#pragma once

#include "mpi.h"

class Topology
{
public:
    virtual int getRank() const
    {
        int rank;
        MPI_Comm_rank(getComm(), &rank);

        return rank;
    }
    virtual MPI_Comm getComm() const = 0;
private:
};

class DefaultTopology : public Topology
{
public:
    MPI_Comm getComm() const
    {
        return MPI_COMM_WORLD;
    }
};