#pragma once

#include "mpi.h"
#include "Topology.h"

class CartesianTopology : public Topology
{
public:
    /**
     * dim : Dimension de la topologie. Pour un probl�me de poisson 2D, dim = 2, pour 3D, dim = 3
     * c : pas utile de le modifier pour le tp
     */
    CartesianTopology(int dim, MPI_Comm c = MPI_COMM_WORLD)
    {
        int pCount;
        MPI_Comm_size(c, &pCount);

        Create(pCount, dim, c);
    }

    int getRank() const
    {
        return rank;
    }

    int getDimensionCount() const
    {
        return dim;
    }

    const int* getDimensions() const
    {
        return dims;
    }

    const int* getCoords() const
    {
        return coords;
    }

    MPI_Comm getComm() const
    {
        return comm;
    }

    /**
     * dimension : dimension sur lequel vous voulez le voisin (axe x = 0, axe y = 1, axz z = 1)
     * direction : amplitude du d�placent. Souvent 1 ou -1. Pour l'axe x, 1 = droite, -1 = gauche, ...
     */
    int getNeighbourRank(int dimension, int direction)
    {
        int tmpRank = getRank();
        int neighbourRank;

        MPI_Cart_shift(comm, dimension, direction, &tmpRank, &neighbourRank);

        return neighbourRank;
    }

    ~CartesianTopology()
    {
        if (dims != nullptr)
            delete[] dims;
        if (dims != nullptr)
            delete[] coords;
    }
private:
    void Create(int processCount, int dim, MPI_Comm c)
    {
        this->dim = dim;

        dims = new int[dim]();
        coords = new int[dim]();

        MPI_Dims_create(processCount, dim, dims);

        int* periods = new int[dim];
        for (int i = 0; i < dim; i++) periods[i] = false;

        MPI_Cart_create(c, dim, dims, periods, true, &comm);
        MPI_Comm_rank(comm, &rank);

        MPI_Cart_coords(comm, rank, dim, coords);
        delete[] periods;
    }
protected:
    int  dim;
    int* dims;
    int* coords;

    int rank;
    MPI_Comm comm;
};
