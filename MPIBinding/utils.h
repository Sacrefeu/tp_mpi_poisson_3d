#pragma once

#include "mpi.h"

double Reduce(double value, MPI_Comm comm, int to = 0, MPI_Op op = MPI_SUM)
{
    double rslt;
    MPI_Reduce(&value, &rslt, 1, MPI_DOUBLE, op, to, comm);

    return rslt;
}