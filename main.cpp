#include "Bench/mpi_lp_3d.h"

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    benchmark_lp3drmaput(1000, 1);

    MPI_Finalize();
    return 0;
}