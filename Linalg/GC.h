#pragma once

#include <vector>

#include "Vector.h"
#include "Matrix.h"

/**
 * \brief Store GC information to faster next calls
 * 
 * \tparam T Field (eg float, double, std::complex, ...)
 * \see GC
 * 
 * At each iteration, this class will store descent directions
 * that can be later used to re-project the initial vector closer
 * to the "real solution" thus gaining order of iterations.
 */
template<typename T>
class GC_History
{
public:
    /**
     * \brief Ctor
     * 
     * \param n The size of the vector (to reduce allocation number)
     * \param max_index The maximum number of descent direction to store
     */
    GC_History(std::size_t n, std::size_t max_index)
    {
        current_index = 0;
        di.resize(max_index, Vector<T>(n, T{}));
        diAdi.resize(max_index, T{});
    }

    /** 
     * \brief Add iteration to the history
     *
     * \param vec The descent direction
     * \param scal <d, Ad> where A is the matrix and d the descent direction
     */
    void add(const Vector<T>& vec, T scal)
    {
        if (di.size() == 0) return;

        di   [current_index %    di.size()] = vec;
        diAdi[current_index % diAdi.size()] = scal;

        current_index++;
    }

    /*!
     * \brief Project the initial vector to a closer approximation of the exact solution
     *
     * If no prior iteration was performed, "to" is returned as-is
     *
     * \param vec The vector to project 
     * \param[out] to The destination vector 
     */
    void project(const Vector<T>& vec, Vector<T>& to)
    {
        if (current_index == 0)
            return;

        const std::size_t end = std::min(current_index, di.size());
        to = (scal(vec, di[0]) / diAdi[0]) * di[0];

        for (std::size_t i = 1; i < end; i++)
            to += (scal(vec, di[i]) / diAdi[i]) * di[i];
    }
private:
    std::size_t current_index; /**< Vector to replace at next iteration*/
     
    std::vector<Vector<T>> di; /**< Descent direction */
    std::vector<T> diAdi; /**< <d, Ad> where d is dhe descent direction and A the matrix */
};

/**
 * \brief Options to gather informations about the GC iterations
 *
 * This class is mostly meaningfull for debugging
 *
 * This enum can be used as a flag to enable multiple options. 
 * Example : options = GC_Reporter_options::PRINT_ERRORS | GC_Reporter_options::PAUSE
 * Will print the error then pause the iteration.
 *
 * \see GC_Reporter
 * \see GC
 */
enum GC_Reporter_options : short int
{
    USELESS      = 0x01, /**< Nothing is gathered */
    STORE_ERRORS = 0x02, /**< Store errors at each iterations */
    STORE_DD     = 0x04, /**< Store descent direction at each iterations */
    PRINT_ERRORS = 0x08, /**< Print the errors to stdout */
    PAUSE        = 0x10  /**< Pause at each iterations */
};

/**
 * \brief  Gather informations about the GC iterations
 *
 * \tparam T Field (eg float, double, std::complex, ...)
 * \see GC_Reporter_Options
 * \see GC
 */
template<typename T>
class GC_Reporter
{
public:
    /**
     * \brief Ctor
     *
     * \param opts The options
     */
    GC_Reporter(short int opts = GC_Reporter_options::STORE_ERRORS)
    {
        options = opts;
    }

    /**
     * \brief Add an iteration to the report
     * 
     * \param dd The descent direction
     * \param error The current error
     */
    void addIt(const Vector<T>& dd, T error)
    {
        if (options & GC_Reporter_options::STORE_DD)     descentDirection.push_back(dd);
        if (options & GC_Reporter_options::STORE_ERRORS) errors.push_back(error);;
        if (options & GC_Reporter_options::PRINT_ERRORS) std::cout << error << std::endl;
        if (options & GC_Reporter_options::PAUSE) std::cin.get();
    }

    /**
     * \brief Returns the descent directions
     *
     * Note : Depending on the options, this vector might be empty
     *
     * \return The descent directions
     */
    const std::vector<Vector<T>>& getDescentDirection() const
    {
        return descentDirection;
    }

    /**
     * \brief Returns the error
     *
     * Note : Depending on the options, this vector might be empty
     *
     * \return The descent directions
     */
    const std::vector<T>& getErrors() const
    {
        return errors;
    }
private:
    short int options; /**< The options (from GC_Reporter_Options) */
    std::vector<Vector<T>> descentDirection; /** The descent directions */
    std::vector<T> errors; /** The errors */
};

/**
 * \brief Perform gradient conjugate method to solve Ax = b
 * 
 * \tparam T Ordered Field (eg float, double, but not std::complex)
 * \tparam Mat Matrix type (must be able to multiply this type by a Vector<T>)
 *
 * \param A The matrix 
 * \param b Right Hand Side
 * \param[in, out] x0 Starting & Resulting vector
 * \param eps Desired precision
 * \param maxIter Maximum number of iteration
 * \param reporter The reporter if needed
 *
 * \see GC_Reporter
 *
 * \return The number of iterations performed (if equal to maxIter, convergence might not have been reached)
 */
template<typename T, typename EPS = T, typename Mat = Matrix<T>>
std::size_t GC(const Mat& A, const Vector<T>& b, Vector<T>& x0, T eps, std::size_t maxIter = 100000, GC_Reporter<T>* reporter = nullptr)
{
    Vector<double> r = b - A * x0;
    Vector<double> d = r;

    std::size_t k = 0;
    T error = scal(r, r);

    while ((error > eps) && (k < maxIter))
    {
        if (reporter) reporter->addIt(d, error);

        Vector<double> Ad = A * d;
        
        T dAd = scal(d, Ad);
        T alpha = error / dAd;

        x0 = x0 + alpha *  d;
        r  =  r - alpha * Ad;

        T error_new = scal(r, r);
        T beta = error_new / error;
        
        d = r + beta * d;
        
        error = error_new;
        k++;
    }

    return k;
}

/**
 * \brief Perform gradient conjugate method to solve Ax = b
 *
 * This uses an history to re-project b into a much closer solution
 *
 * \tparam T Ordered Field (eg float, double, but not std::complex)
 * \tparam Mat Matrix type (must be able to multiply this type by a Vector<T>)
 *
 * \param history The history of previous iterations
 * \param A The matrix
 * \param b Right Hand Side
 * \param[in, out] x0 Starting & Resulting vector
 * \param eps Desired precision
 * \param maxIter Maximum number of iteration
 * \param reporter The reporter if needed
 *
 * \see GC_Reporter
 * \see GC_History
 *
 * \return The number of iterations performed (if equal to maxIter, convergence might not have been reached)
 */
template<typename T, typename Mat = Matrix<T>>
std::size_t GC(GC_History<T>& history, const Mat& A, const Vector<T>& b, Vector<T>& x0, T eps, std::size_t maxIter = 100000, GC_Reporter<T>* reporter = nullptr)
{
    history.project(b, x0);
    Vector<double> r = b - A * x0;
    Vector<double> d = r;

    std::size_t k = 0;
    T error = scal(r, r);

    while ((error > eps) && (k < maxIter))
    {
        if (reporter) reporter->addIt(d, error);

        Vector<double> Ad = A * d;
   
        T dAd = scal(d, Ad);
        history.add(d, dAd);

        T alpha = error / dAd;
        x0 = x0 + alpha *  d;
        r  =  r - alpha * Ad;

        T error_new = scal(r, r);
        T beta = error_new / error;

        d = r + beta * d;
        
        error = error_new;
        k++;
    }

    return k;
}