#pragma once

#include <vector>
#include <utility>
#include <algorithm>
#include <fstream>
#include "Vector.h"

/**
 * \brief Matrix
 * 
 * \tparam T Field (eg float, double, std::complex, ...)
 */
template<typename T>
class Matrix
{
public:
    /**
     * \brief Create empty matrix
     * 
     * \param n The number of rows
     * \param m The number of columns
     */
    inline Matrix(std::size_t n, std::size_t m)
    {
        n = m = 0;
        data = nullptr;
        realloc(n, m);
    }

    /**
     * \brief Create matrix filled with a value
     *
     * \param n The number of rows
     * \param m The number of columns
     * \param value The value to fill
     */
    inline Matrix(std::size_t n, std::size_t m, T value)
    {
        n = m = 0;
        data = nullptr;
        realloc(n, m, value);
    }

    /**
     * \brief Create matrix from a diagonal
     *
     * The size of the matrix is infered from the parameter

     * \param diag The matrix diagonal
     */
    inline Matrix(const Vector<T>& diag)
    {
        n = m = 0;
        data = nullptr;
        realloc(diag.dim(), diag.dim());

        for (std::size_t i = 0; i < n; i++)
            data[i][i] = diag[i];
    }

    /**
     * \brief Copy constructor
     *
     * \param other The matrix to be copied from
     */
    inline Matrix(const Matrix<T>& other)
    {
        n = m = 0;
        data = nullptr;
        realloc(other.n, other.m);

        std::copy(other.data, other.data + n, data);
    }

    /**
     * \brief Move constructor
     *
     * \param other The matrix to be moved from
     */
    inline Matrix(Matrix&& other)
    {
        data = other.data;
        other.data = nullptr;
        
        n = other.n;
        m = other.m;
    }
    
    /**
     * \brief Assignment operator
     *
     * \param other The matrix to be copied from
     */
    inline Matrix<T>& operator=(const Matrix& other)
    {
        realloc(other.n, other.m);
        std::copy(other.data, other.data + n, data);

        return *this;
    }

    /**
     * \brief Assignment operator
     *
     * \param other The matrix to be moved from
     */
    inline Matrix<T>& operator=(Matrix&& other)
    {
        data = other.data;
        other.data = nullptr;

        n = other.n;
        m = other.m;

        return *this;
    }

    /**
     * \brief Returns matrix dimension
     *
     * \return The matrix dimensions
     */
    inline std::pair<std::size_t, std::size_t> dims() const
    {
        return { n, m };
    }

    /**
     * \brief Returns the i-th row
     *
     * \return The i-th row
     */
    inline Vector<T> operator[](std::size_t i) const
    {
        return data[i];
    }

    /**
     * \brief Returns the i-th row
     *
     * \return The i-th row
     */
    inline Vector<T>& operator[](std::size_t i)
    {
        return data[i];
    }

    /**
     * \brief Returns the transposed matrix
     *
     * \param rhs The right hand side of the operation
     *
     * \return The transposed matrix
     */
    inline Matrix<T> transpose() const
    {
        const auto dims = this->dims();

        Matrix<T> transpose(dims.second, dims.first);
        for (std::size_t i = 0; i < dims.first; i++)
        {
            for (std::size_t j = 0; j < dims.second; j++)
            {
                transpose[j][i] = (*this)[i][j];
            }
        }
        return transpose;
    }

    /**
     * \brief Adds two matrices
     *
     * Note : if matrices does not have compatible shape, the addition
     *        is performed on the biggest subset possible
     *
     * \param rhs The right hand side of the operation
     *
     * \return The matrix to add to the current matrix
     */
    inline Matrix<T> operator+(const Matrix<T>& rhs) const
    {
        const std::size_t n = std::min(rhs.dims().first, this->dims().first);
        const std::size_t m = std::min(rhs.dims().second, this->dims().second);
    
        Matrix<T> result(n, m);
        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                result[i][j] = (*this)[i][j] + rhs[i][j];
            }
        }

        return result;
    }

    /**
     * \brief Adds two matrices (inplace)
     *
     * Note : if matrices does not have compatible shape, the addition
     *        is performed on the biggest subset possible
     *
     * \param rhs The right hand side of the operation
     *
     * \return The matrix to add to the current matrix
     */
    inline Matrix<T>& operator+=(const Matrix<T>& rhs)
    {
        const std::size_t n = std::min(rhs.dims().first, this->dims().first);
        const std::size_t m = std::min(rhs.dims().second, this->dims().second);

        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                (*this)[i][j] += rhs[i][j];
            }
        }

        return *this;
    }

    /**
     * \brief Substract two matrices
     *
     * Note : if matrices does not have compatible shape, the substraction
     *        is performed on the biggest subset possible
     *
     * \param rhs The right hand side of the operation
     *
     * \return The matrix to substract from the current matrix
     */
    inline Matrix<T> operator-(const Matrix<T>& rhs) const
    {
        const std::size_t n = std::min(rhs.dims().first, this->dims().first);
        const std::size_t m = std::min(rhs.dims().second, this->dims().second);

        Matrix<T> result(n, m);
        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                result[i][j] = (*this)[i][j] - rhs[i][j];
            }
        }

        return result;
    }

    /**
     * \brief Substract two matrices (inplace)
     * 
     * Note : if matrices does not have compatible shape, the substraction
     *        is performed on the biggest subset possible
     * \param rhs The right hand side of the operation
     *
     * \return The matrix to substract from the current matrix
     */
    inline Matrix<T>& operator-=(const Matrix<T>& rhs)
    {
        const std::size_t n = std::min(rhs.dims().first, this->dims().first);
        const std::size_t m = std::min(rhs.dims().second, this->dims().second);

        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                (*this)[i][j] -= rhs[i][j];
            }
        }

        return *this;
    }

    /**
     * \brief Multiply matrix by a number
     *
     * \param value The right hand side of the operation
     *
     * \return The matrix multiplied by a value
     */
    inline Matrix<T> operator*(T value) const
    {
        const std::size_t n = dims().first;
        const std::size_t m = dims().second;

        Matrix<T> result = *this;
        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                result[i][j] *= value;
            }
        }

        return result;
    }

    /**
     * \brief Multiply matrix by a number (inplace)
     *
     * \param value The right hand side of the operation
     *
     * \return The matrix multiplied by a value
     */
    inline Matrix<T>& operator*=(T value)
    {
        const std::size_t n = dims().first;
        const std::size_t m = dims().second;

        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                (*this)[i][j] *= value;
            }
        }

        return *this;
    }

    /**
     * \brief Divide matrix by a number
     *
     * \param value The right hand side of the operation
     *
     * \return The matrix divided by a value
     */
    inline Matrix<T> operator/(T value) const
    {
        const std::size_t n = dims().first;
        const std::size_t m = dims().second;

        Matrix<T> result = *this;
        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                result[i][j] /= value;
            }
        }

        return result;
    }

    /**
     * \brief Divide matrix by a number (inplace)
     *
     * \param value The right hand side of the operation
     *
     * \return The matrix divided by a value
     */
    inline Matrix<T>& operator/=(T value)
    {
        const std::size_t n = dims().first;
        const std::size_t m = dims().second;

        for (std::size_t i = 0; i < n; i++)
        {
            for (std::size_t j = 0; j < m; j++)
            {
                (*this)[i][j] /= value;
            }
        }

        return *this;
    }

    /**
     * \brief Matrix-Vector multiplication
     * 
     * Note : if matrix and vector does not have compatible shape,
     *        the multiplication is performed on the biggest subset possible
     * \param value The right hand side of the operation
     *
     * \return The result of matrice vector multiplication
     */
    inline Vector<T> operator*(const Vector<T>& vec) const
    {
        std::size_t n = this->dims().first;
        std::size_t m = std::min(this->dims().second, vec.dim());

        Vector<T> result(n);
        for (std::size_t i = 0; i < n; i++)
        {
            T value{};
            for (std::size_t j = 0; j < m; j++)
            {
                value += (*this)[i][j] * vec[j];
            }
            result[i] = value;
        }
        return result;
    }

    /**
     * \brief Dctor
     */
    inline ~Matrix()
    {
        if (data != nullptr)
            delete[] data;
    }
private:
    /**
     * \brief Realloc matrix data
     *
     * If size is not changed, no realocation is performed at all
     *
     * \param N The new number of rows
     * \param M The new number of columns
     * \param T The value to fill 
     */
    void realloc(std::size_t N, std::size_t M, T value = T{})
    {
        if (n == N && m == M) return;

        if (data != nullptr)
            delete[] data;

        n = N;
        m = M;

        data = new Vector<T>[N];
        for (std::size_t i = 0; i < N; i++)
            data[i] = Vector<T>(M, value);
    }

    std::size_t n; /**< The number of rows */
    std::size_t m; /**< The number of columns */
    Vector<T>* data; /**< Actual matric coefficient */
};


/**
 * \brief Multiply matrix by a number
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param value The left hand side of the operation
 * \param mat The matrix 
 * 
 * \return The matrix multiplied by a value
 */
template<typename T>
inline Matrix<T> operator*(T value, const Matrix<T>& mat)
{
    const std::size_t n = mat.dims().first;
    const std::size_t m = mat.dims().second;

    Matrix<T> result = mat;
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < m; j++)
        {
            result[i][j] *= value;
        }
    }

    return result;
}

/**
 * \brief Divide matrix by a number
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param value The left hand side of the operation
 * \param mat The matrix
 *
 * \return The matrix divided by a value
 */
template<typename T>
inline Matrix<T> operator/(T value, const Matrix<T>& mat)
{
    const std::size_t n = mat.dims().first;
    const std::size_t m = mat.dims().second;

    Matrix<T> result = mat;
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < m; j++)
        {
            result[i][j] /= value;
        }
    }

    return result;
}

/**
 * \brief Matrix-Matrix multiplication
 *
 * Note : if matrices does not have compatible shape, the multiplication
 *        is performed on the biggest subset possible
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param rhs The right hand side of the operations
 * \param lhs The left hand side of the operation
 *
 * \return The result of matrix vector multiplication
 */
template<typename T>
inline Matrix<T> operator*(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    std::size_t n = lhs.dims().first;
    std::size_t m = rhs.dims().second;

    std::size_t l = std::min(lhs.dims().second, rhs.dims().first);

    Matrix<T> result(n, m);
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < m; j++)
        {
            T value{};

            for (std::size_t k = 0; k < l; k++)
            {
                value += lhs[i][k] * rhs[k][j];
            }

            result[i][j] = value;
        }
    }

    return result;
}

/**
 * \brief Print matrix to stream
 * 
 * Each rows start and ends with a parenthesis
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param stream The stream to print matrix into
 * \param mat The matrix to print
 *
 * \return The stream
 */
template<typename T>
inline std::ostream& operator<<(std::ostream& stream, const Matrix<T>& mat)
{
    for (std::size_t i = 0; i < mat.dims().first; i++)
        stream << mat[i] << "\n";
    return stream;
}

/**
 * \brief Print matrix to a file stream
 *
 * The rows does not include the parenthesis (CSV format)
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param stream The stream to print matrix into
 * \param mat The matrix to print
 *
 * \return The stream
 */
template<typename T>
inline std::ofstream& operator<<(std::ofstream& stream, const Matrix<T>& mat)
{
    for (std::size_t i = 0; i < mat.dims().first; i++)
    {
        for (std::size_t j = 0; j < mat.dims().second -1; j++)
        {
            stream << mat[i][j] << ",";
        }
        stream << mat[i][mat.dims().second - 1];
        stream << "\n";
    }

    return stream;
}

/**
 * \brief Saves matrix to a CSV file
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param fileName The name of the file
 * \param mat The matrix to save
 *
 */
template<typename T>
inline void toCSVFile(std::string fileName, const Matrix<T>& mat)
{
    std::ofstream file;
    file.open(fileName);
    file << mat;
    file.close();
}