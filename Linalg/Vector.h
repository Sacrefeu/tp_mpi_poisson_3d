#pragma once

#include <vector>
#include <algorithm>
#include <utility>
#include <algorithm>

#include <iostream>

/**
 * \brief Vector class
 * 
 * \tparam T Field (eg float, double, std::complex, ...)
 */
template<typename T>
class Vector
{
public:
    /**
     * \brief Default constructor
     */
    inline Vector()
    {
        data = nullptr;
        n = 0;
    }

    /** 
     * \brief Constrcts the null vector
     *
     * \param size The dimension of the vector
     */
    inline Vector(std::size_t size)
    {
        data = nullptr;
        n = 0;
        realloc(size);
    }

    /**
     * \brief Constrcts a vector filled with a value
     *
     * \param size The dimension of the vector
     * \param value The value to be filled
     */
    inline Vector(std::size_t size, T value)
    {
        data = nullptr;
        n = 0;
        realloc(size);

        std::fill(data, data + n, value);
    }

    /**
     * \brief Construct a vector from a standard std::vector
     *
     * \param d The std::vector to construct from
     */
    inline Vector(const std::vector<T>& d)
    {
        data = nullptr;
        n = 0;
        realloc(d.size());

        std::copy(d.begin(), d.end(), data);
    }

    /**
     * \brief Construct a vector from an std::initializer_list
     *
     * This allows to construct vectors on the flight 
     * Example : Vector<double> b = {1.0, 1.0, 1.0, 1.0};
     *
     * \param list The std::initializer_list to construct from
     */
    inline Vector(const std::initializer_list<T>& list)
    {
        data = nullptr;
        n = 0;
        realloc(list.size());

        std::copy(list.begin(), list.end(), data);
    }

    /**
     * \brief Copy constructor
     * 
     * \param v The vector to be copied from
     */
    inline Vector(const Vector& v)
    {
        data = nullptr;
        n = 0;
        realloc(v.size());

        std::copy(v.data, v.data + n, data);
    }

    /**
     * \brief Move constructor
     *
     * \param v The vector to be moved from
     */
    inline Vector(Vector&& v)
    {
        n = v.n;
        data = v.data;
        v.data = nullptr;
    }

    /**
     * \brief Assignement operator
     *
     * \param vec The std::vector to be copied from
     */
    inline Vector& operator=(const std::vector<T>& vec)
    {
        realloc(vec.size());
        std::copy(vec.begin(), vec.end(), data);
        return *this;
    }

    /**
     * \brief Assignement operator
     *
     * \param l The std::initialize_list to be copied from
     */
    inline Vector& operator=(const std::initializer_list<T>& l)
    {
        realloc(l.size());
        std::copy(l.begin(), l.end(), data);
        return *this;
    }

    /**
     * \brief Assignement operator
     *
     * \param vec The Vector to be copied from
     */
    inline Vector& operator=(const Vector<T>& vec)
    {
        realloc(vec.size());
        std::copy(vec.data, vec.data + n, data);
        return *this;
    }

    /**
     * \brief Assignement operator
     *
     * \param vec The vector to be moved from
     */
    inline Vector& operator=(Vector<T>&& vec)
    {
        if (data != nullptr)
            delete[] data;

        n = vec.n;
        data = vec.data;
        vec.data = nullptr;
        return *this;
    }

    /**
     * \brief Returns the dimension of the vector
     * 
     * \return The dimension of the vector
     */
    inline std::size_t size() const
    {
        return n;
    }

    /**
     * \brief Returns the dimension of the vector
     *
     * \return The dimension of the vector
     */
    inline std::size_t dim() const
    {
        return size();
    }

    /**
     * \brief Returns the i-th component of the vector
     *
     * \return The i-th component of the vector
     */
    inline T operator[](std::size_t i) const
    {
        return data[i];
    }


    /**
     * \brief Returns the i-th component of the vector
     *
     * \return The i-th component of the vector
     */
    inline T& operator[](std::size_t i)
    {
        return data[i];
    }

    inline const T* at(std::size_t i) const
    {
        return &data[i];
    }

    inline T* at(std::size_t i)
    {
        return &data[i];
    }

    /**
     * \brief Adds two vectors
     *
     * \param rhs The right hand side of the operation
     *
     * \return Rhs added to the current vector
     */
    inline Vector<T> operator+(const Vector<T>& rhs) const
    {
        const std::size_t dim = std::min(rhs.dim(), this->dim());
        
        Vector<T> result(dim);
        for (std::size_t i = 0; i < dim; i++)
            result[i] = (*this)[i] + rhs[i];

        return result;
    }

    /**
     * \brief Adds two vectors (inplace)
     *
     * \param rhs The right hand side of the operation
     *
     * \return Rhs added to the current vector
     */
    inline Vector<T>& operator+=(const Vector<T>& rhs)
    {
        const std::size_t dim = std::min(rhs.dim(), this->dim());

        for (std::size_t i = 0; i < dim; i++)
            (*this)[i] += rhs[i];

        return *this;
    }
    
    /**
     * \brief Substracts two vectors
     *
     * \param rhs The right hand side of the operation
     *
     * \return The current vector substracted from rhs
     */
    inline Vector<T> operator-(const Vector<T>& rhs) const
    {
        const std::size_t dim = std::min(rhs.dim(), this->dim());

        Vector<T> result(dim);
        for (std::size_t i = 0; i < dim; i++)
            result[i] = (*this)[i] - rhs[i];

        return result;
    }

    /**
     * \brief Substracts two vectors (inplace)
     *
     * \param rhs The right hand side of the operation
     *
     * \return The current vector substracted from rhs
     */
    inline Vector<T>& operator-=(const Vector<T>& rhs)
    {
        const std::size_t dim = std::min(rhs.dim(), this->dim());

        for (std::size_t i = 0; i < dim; i++)
            (*this)[i] -= +rhs[i];

        return *this;
    }

    /**
     * \brief Multiply a vector by a value
     *
     * \param value The value to multiply the current vector by
     *
     * \return The current vector multtiplied by value
     */
    inline Vector<T> operator*(T value) const
    {
        const std::size_t dim = this->dim();
        
        Vector<T> result = *this;
        for (std::size_t i = 0; i < dim; i++)
            result[i] *= value;

        return result;
    }

    /**
     * \brief Multiply a vector by a value (inplace)
     *
     * \param value The value to multiply the current vector by
     *
     * \return The current vector multtiplied by value
     */
    inline Vector<T>& operator*=(T value)
    {
        const std::size_t dim = dim();

        for (std::size_t i = 0; i < dim; i++)
            (*this)[i] *= value;

        return *this;
    }

    /**
     * \brief Divide a vector by a value
     *
     * \param value The value to divide the current vector by
     *
     * \return The current vector divided by value
     */
    inline Vector<T> operator/(T value) const
    {
        const std::size_t dim = this->dim();

        Vector<T> result = *this;
        for (std::size_t i = 0; i < dim; i++)
            result[i] /= value;

        return result;
    }

    /**
     * \brief Divide a vector by a value (inplace)
     *
     * \param value The value to divide the current vector by
     *
     * \return The current vector divided by value
     */
    inline Vector<T>& operator/=(T value)
    {
        const std::size_t dim = dim();

        for (std::size_t i = 0; i < dim; i++)
            (*this)[i] /= value;

        return *this;
    }

    /**
     * \brief Dctor
     */
    inline ~Vector()
    {
        if (data != nullptr)
            delete[] data;
    }
private:
    /**
     * \brief Reallocates the data for the vector
     * 
     * If the size hasn't changed, no reallocation is performed
     *
     * \param N The new size of the vector
     */
    void realloc(std::size_t N)
    {
        if (n == N) return;

        if (data != nullptr)
            delete[] data;

        data = new double[N]();
        n = N;
    }

    std::size_t n; /**< The size of the vector */
    T* data; /**< The coefficients of the vector */
};

/**
 * \brief Multiply a vector by a value
 *
 * \tparam T Field (float, double, std::complex, ...)
 *
 * \param value The value to multiply the vector by
 * \param vec The vector
 *
 * \return The vector multtiplied by value
 */
template<typename T>
inline Vector<T> operator*(T value, const Vector<T>& vec)
{
    const std::size_t dim = vec.dim();

    Vector<T> result = vec;
    for (std::size_t i = 0; i < dim; i++)
        result[i] *= value;

    return result;
}

/**
 * \brief Divide a vector by a value
 *
 * \tparam T Field (float, double, std::complex, ...)
 *
 * \param value The value to divide the vector by
 * \param vec The vector
 *
 * \return The vector multtiplied by value
 */
template<typename T>
inline Vector<T> operator/(T value, const Vector<T>& vec)
{
    const std::size_t dim = vec.dim();

    Vector<T> result = vec;
    for (std::size_t i = 0; i < dim; i++)
        result[i] /= value;

    return result;
}

/**
 * \brief Perform Scalar Product of two vector
 * 
 * Note : this is not hermitian
 * 
 * \tparam T Field (float, double, std::complex, ...)
 *
 * \param lhs The left  hand side vector
 * \param rhs The right hand side vector
 *
 * \return The scalar product
 */
template<typename T>
inline T scal(const Vector<T>& lhs, const Vector<T>& rhs)
{
    const std::size_t dim = std::min(lhs.dim(), rhs.dim());

    T result{};
    for (std::size_t i = 0; i < dim; i++)
        result += lhs[i] * rhs[i];

    return result;
}

/**
 * \brief Print vector to stream
 *
 * The line starts and ends with a parenthesis
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param stream The stream to print Vector into
 * \param vec The Vector to print
 *
 * \return The stream
 */
template<typename T>
inline std::ostream& operator<<(std::ostream& stream, const Vector<T>& vec)
{
    stream << "(";
    for (std::size_t i = 0; i < vec.dim() - 1; i++)
    {
        stream << vec[i] << ", ";
    }
    stream << vec[vec.dim() - 1] << ")";
    return stream;
}

/**
 * \brief Print Vector to a file stream
 *
 * The line does not include the parenthesis (CSV format)
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param stream The stream to print matrix into
 * \param vec The Vector to print
 *
 * \return The stream
 */
template<typename T>
inline std::ofstream& operator<<(std::ofstream& stream, const Vector<T>& vec)
{
    for (std::size_t i = 0; i < vec.dim() - 1; i++)
        stream << vec[i] << ",";
    stream << vec[vec.dim() - 1];

    return stream;

}

/**
 * \brief Saves vector to a CSV file
 *
 * \tparam Field (eg float, double, std::complex, ...)
 *
 * \param fileName The name of the file
 * \param vec The Vector to save
 *
 */
template<typename T>
inline void toCSVFile(const std::string& fileName, const Vector<T>& vec)
{
    std::ofstream file;
    file.open(fileName);
    file << vec;
    file.close();
}