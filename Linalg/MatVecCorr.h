#pragma once

#include "Matrix.h"
#include "Vector.h"

/**
 * \brief Reshape vector to matrix
 * 
 * Note : If the size of the vector is not a multiple of numRows, 
 *        the vector will be troncated to the biggest multiple of numRows
 * 
 * \tparam T Filed (eg float, double, std::complex, ...)
 * 
 * \param Vec the vector to reshape
 * \param numRows the number of rows
 *
 * \return The reshaped vector as matrix
 */
template<typename T>
Matrix<T> toMatrix(const Vector<T>& vec, std::size_t numRows)
{
    const std::size_t numCols = (std::size_t)std::ceil((double)vec.dim() / (double)numRows);
    
    Matrix<T> result(numRows, numCols);
    
    for (std::size_t i = 0; i < numRows; i++)
    {
        for (std::size_t j = 0; j < numCols; j++)
        {
            result[i][j] = vec[j + i * numCols];
        }
    }

    return result;
}

/**
 * \brief Flatten matrix 
 *
 * \tparam T Filed (eg float, double, std::complex, ...)
 *
 * \param mat The matrix to flatten
 *
 * \return The flattened matrix
 */
template<typename T>
Vector<T> toVector(const Matrix<T>& mat)
{
    Vector<T> result(mat.dims().first * mat.dims().second);

    for (std::size_t i = 0; i < mat.dims().first; i++)
    {
        for (std::size_t j = 0; j < mat.dims().second; j++)
        {
            result[i * mat.dims().second + j] = mat[i][j];
        }
    }

    return result;
}