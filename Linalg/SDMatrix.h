#pragma once

#include "Matrix.h"
#include "Vector.h"

/**
 * \brief Symetric diagonally stored matrices
 * 
 * \tparam T Field (eg float, double, std::complex, ...)

 * Symetric matrices where only some of its diagonals are stored. Not all
 * classical operations are provided. Only matrix/vector multiplication is
 */
template<typename T>
class SDMatrix
{
public:
    /**
     * \brief Create empty matrix
     *
     * \param n The number of rows and colmuns
     * \param d The number of diagonal to store
     */
    inline SDMatrix(std::size_t N, std::size_t d)
    {
        data = nullptr;
        offsets = nullptr;
        n = 0;
        noffsets = 0;

        realloc_data(d, N);
        realloc_offsets(d);
    }

    /**
     * \brief Create matrix filled with a value
     *
     * \param n The number of rows and colmuns
     * \param d The number of diagonal to store
     * \param value The value to fill
     */
    inline SDMatrix(std::size_t N, std::size_t d, T value)
    {
        data = nullptr;
        offsets = nullptr;
        n = 0;
        noffsets = 0;

        realloc_data(d, N, value);
        realloc_offsets(d);
    }

    /**
     * \brief Copy constructor
     *
     * \param matrix The matrix to be copied from
     */
    inline SDMatrix(const SDMatrix<T>& matrix)
    {
        data = nullptr;
        offsets = nullptr;
        n = 0;
        noffsets = 0;

        realloc_data(matrix.noffsets, matrix.n);
        realloc_offsets(matrix.noffsets);

        for (std::size_t i = 0; i < noffsets; i++)
        {
            offsets[i] = matrix.offsets[i];
            data[i] = matrix.data[i];
        }
    }

    /**
     * \brief Copy constructor
     *
     * \param matrix The matrix to be copied from
     */
    inline SDMatrix(SDMatrix<T>&& matrix)
    {
        data    = matrix.data;
        offsets = matrix.offsets;

        matrix.data = nullptr;
        matrix.offsets = nullptr;

        n = matrix.n;
        noffsets = matrix.noffsets;
    }

    /**
     * \brief Assignment operator
     *
     * \param matrix The matrix to be moved from
     */
    SDMatrix<T>& operator=(SDMatrix&& matrix)
    {
        if (data != nullptr)
            delete[] data;
        if (offsets != nullptr)
            delete[] offsets;

        data = matrix.data;
        offsets = matrix.offsets;

        matrix.data = nullptr;
        matrix.offsets = nullptr;

        n = matrix.n;
        noffsets = matrix.noffsets;

        return *this;
    }

    /**
     * \brief Assignment operator
     *
     * \param matrix The matrix to be copied from
     */
    SDMatrix<T>& operator=(const SDMatrix& matrix)
    {
        realloc_data(matrix.noffsets, matrix.n);
        realloc_offsets(matrix.noffsets);

        for (std::size_t i = 0; i < noffsets; i++)
        {
            offsets[i] = matrix.offsets[i];
            data[i] = matrix.data[i];
        }

        return *this;
    }

    /**
     * \brief Sets diagonal offsets
     *
     * WARNING : THIS FUNCTIONS INVALIDATE PREVIOUS COEFFICIENTS
     *
     * \params off The new offsets
     */
    inline void setOffsets(const std::vector<std::size_t>& off)
    {
        realloc_data(off.size(), n, T{});
        realloc_offsets(off.size());

        std::copy(off.begin(), off.end(), offsets);

        auto id0ptr = std::find(off.begin(), off.end(), 0);

        if (id0ptr != off.end())
            id0 = std::distance(off.begin(), id0ptr);
        else
            id0 = off.size();
    }

    /**
     * \brief Returns the dimension
     *
     * \return The dimension
     */
    inline std::size_t getDimension() const
    {
        return n;
    }

    /**
     * \brief Returns the number of stored diagonal
     *
     * \return The number of stored diagonal
     */
    inline std::size_t getDiagonalCount() const
    {
        return noffsets;
    }

    /**
     * \brief Returns the offsets of diagonals
     *
     * \return The offsets of diagonals
     */
    inline const std::size_t* getOffsets() const
    {
        return offsets;
    }

    /**
     * \brief Returns the getOffsets()[i]-th diagonal
     *
     * \param i The wanted offseted diagonal
     *
     * \return The getOffsets()[i]-th diagonal
     */
    inline const Vector<T>& operator[](std::size_t i) const
    {
        return data[i];
    }

    /**
     * \brief Returns the getOffsets()[i]-th diagonal
     *
     * \param i The wanted offseted diagonal
     *
     * \return The getOffsets()[i]-th diagonal
     */
    inline Vector<T>& operator[](std::size_t i)
    {
        return data[i];
    }

    /**
     * \brief Return the main diagonal index in getOffsets()
     * 
     * This number might be invalide (ie greater than getDiagonalCount())
     * if the main diagonal is not stored
     *
     * \return The index of the main diagonal
     */
    inline std::size_t getMainDiagonal() const
    {
        return id0;
    }

    /**
     * \brief Convert the matrix to plain matrix
     *
     * \return The same matrix stored conventionnaly
     */
    inline Matrix<T> toPlainMatrix() const
    {
        const std::size_t N = getDimension();
        
        Matrix<T> result(N, N, T{});
        
        for (std::size_t i = 0; i < getDiagonalCount(); i++)
        {
            for (std::size_t j = 0; j < N - offsets[i]; j++)
            {
                result[j][j + offsets[i]] = data[i][j];
            }
            for (std::size_t j = offsets[i]; j < N; j++)
            {
                result[j][j - offsets[i]] = data[i][j - offsets[i]];
            }
        }
        
        return result;
    }

    /**
     * \brief Dctor
     */
    ~SDMatrix()
    {
        if (data != nullptr)
            delete[] data;

        if (offsets != nullptr)
            delete[] offsets;
    }
 private:
    /**
     * \brief Reallocates coefficients
     * 
     * If the number of diagonal hasn't changed, no realocation is performed
     *
     * \param d The new number of diagonal
     * \param N The new size of the matrix
     * \param value The value to fill
     */
    inline void realloc_data(std::size_t d, std::size_t N, T value = {})
    {
        if (d == noffsets) return;

        if (data != nullptr)
            delete[] data;

        n = N;

        data = new Vector<T>[d];
        
        for (std::size_t i = 0; i < d; i++)
            data[i] = Vector<T>(N, value);
    }

    /**
     * \brief Reallocates offsets
     *
     * If the number of diagonal hasn't changed, no realocation is performed
     *
     * \param N The new number of diagonal
     */
    inline void realloc_offsets(std::size_t N)
    {
        if (noffsets == N) return;

        if (offsets != nullptr)
            delete[] offsets;

        noffsets = N;
        offsets = new std::size_t[N]();
        id0 = N;
    }

    Vector<T>* data; /**< Diagonals */
    std::size_t* offsets; /**< offsets of diagonals */
    std::size_t noffsets; /**< Number of diagonals */
    std::size_t n; /**< Size of the matrix */
    std::size_t id0; /** Index of the main diagonal */
};

/**
 * \brief Perform Matrix/Vector multiplication
 *
 * \tparam T Field (eg float, double, std::complex, ...)
 * \param mat The matrix
 * \param vec The vector
 *
 * \return The matrix-vector multiplication
 */
template<typename T>
inline Vector<T> operator*(const SDMatrix<T>& mat, const Vector<T>& vec)
{
    const std::size_t* offsets   = mat.getOffsets();
    const std::size_t  diagCount = mat.getDiagonalCount();
    const std::size_t N   = std::min(mat.getDimension(), vec.dim());
    const std::size_t id0 = mat.getMainDiagonal();
    
    Vector<T> v(N, T{});

    for (std::size_t d = 0; d < id0; d++)
    {
        const std::size_t offset = offsets[d];
        const auto maxp = std::min(offset, N - offset);
        const auto minm = std::max(offset, N - offset);

        for (std::size_t i = 0; i < maxp; i++)
            v[i] += mat[d][i] * vec[i + offset];

        for (std::size_t i = offset; i <= N - offset - 1; i++)
        {
            v[i] += mat[d][i] * vec[i + offset];
            v[i] += mat[d][i - offset] * vec[i - offset];
        }

        for (std::size_t i = minm; i < N; i++)
            v[i] += mat[d][i - offset] * vec[i - offset];
    }

    for (std::size_t i = 0; i < N; i++)
    {
        v[i] += mat[id0][i] * vec[i];
    }

    for (std::size_t d = id0 + 1; d < diagCount; d++)
    {
        const std::size_t offset = offsets[d];
        const auto maxp = std::min(offset, N - offset);
        const auto minm = std::max(offset, N - offset);

        for (std::size_t i = 0; i < maxp; i++)
            v[i] += mat[d][i] * vec[i + offset];

        for (std::size_t i = offset; i <= N - offset - 1; i++)
        {
            v[i] += mat[d][i] * vec[i + offset];
            v[i] += mat[d][i - offset] * vec[i - offset];
        }

        for (std::size_t i = minm; i < N; i++)
            v[i] += mat[d][i - offset] * vec[i - offset];
    }

    return v;
}
