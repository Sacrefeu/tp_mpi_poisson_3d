#pragma once

#include "../Linalg/linalg.h"
#include "../Mesh/CartesianMesh.h"

/**
 * \brief Return 1D Laplacian matrix
 * 
 * \param n The dimension 
 *
 * \return The 1D Laplacian matrix
 */
template<typename T>
SDMatrix<T> getLaplacian1D(Grid1D<T>* mesh)
{
    const std::size_t n = mesh->getXDimension();
    const double ihx2 = 1.0 / (mesh->gethx() * mesh->gethx());

    SDMatrix<T> lp(n, 2);
    lp.setOffsets({ 0, 1 });
    lp[0] = Vector<T>(n - 0, -2 * ihx2);
    lp[1] = Vector<T>(n - 1,  1 * ihx2);

    return lp;
}

/**
 * \brief Return 2D Laplacian matrix
 *
 * \param n The x-dimension
 * \param m The y-dimension
 *
 * \return The 2D Laplacian matrix
 */
template<typename T>
SDMatrix<T> getLaplacian2D(Grid2D<T>* mesh)
{
    const std::size_t n = mesh->getXDimension();
    const std::size_t m = mesh->getYDimension();
    const double ihx2 = 1.0 / (mesh->gethx() * mesh->gethx());
    const double ihy2 = 1.0 / (mesh->gethy() * mesh->gethy());

    SDMatrix<T> lp(n * m, 3);
    lp.setOffsets({ 0, 1, n });
    lp[0] = Vector<T>(n * m - 0, -2 * (ihx2 + ihy2));
    lp[1] = Vector<T>(n * m - 1,  1 * ihx2);
    lp[2] = Vector<T>(n * m - n,  1 * ihy2);

    return lp;
}

/**
 * \brief Return 2D Laplacian matrix
 *
 * \param n The x and y-dimensions
 *
 * \return The 2D Laplacian matrix
 */
template<typename T>
SDMatrix<T> getLaplacian2D(std::size_t n)
{
    return getLaplacian2D<T>(n, n);
}

/**
 * \brief Return 3D Laplacian matrix
 *
 * \param n The x-dimension
 * \param m The y-dimension
 * \param k The y-dimension
 *
 * \return The 3D Laplacian matrix
 */
template<typename T>
SDMatrix<T> getLaplacian3D(Grid3D<T>* mesh)
{
    const std::size_t n = mesh->getXDimension();
    const std::size_t m = mesh->getYDimension();
    const std::size_t k = mesh->getZDimension();

    const double ihx2 = 1.0 / (mesh->gethx() * mesh->gethx());
    const double ihy2 = 1.0 / (mesh->gethy() * mesh->gethy());
    const double ihz2 = 1.0 / (mesh->gethz() * mesh->gethz());


    SDMatrix<T> lp(n * m * k, 4);
    lp.setOffsets({ 0, 1, n, n * m });
    lp[0] = Vector<T>(n * m * k - 0,    -2 * (ihx2 + ihy2 + ihz2));
    lp[1] = Vector<T>(n * m * k - 1    , 1 * ihx2);
    lp[2] = Vector<T>(n * m * k - n    , 1 * ihy2);
    lp[3] = Vector<T>(n * m * k - n * m, 1 * ihz2);

    return lp;
}

/**
 * \brief Return 3D Laplacian matrix
 *
 * \param n The x, y and z-dimensions
 *
 * \return The 3D Laplacian matrix
 */
template<typename T>
SDMatrix<T> getLaplacian3D(std::size_t n)
{
    return getLaplacian3D<T>(n, n, n);
}
