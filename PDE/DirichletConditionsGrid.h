#pragma once

#include "../Linalg/linalg.h"
#include "BoundaryConditions.h"
#include "../Mesh/CartesianMesh.h"

/**
 * \brief Dirichlet condition on 1D Grid Mesh
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class DirichletConditionsGrid1D : public BoundaryConditions<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param m The grid to apply conditions on
     */
    DirichletConditionsGrid1D(Grid1D<T>* m)
    {
        mesh = m;
        if (mesh != nullptr)
            conditions = Vector<T>(mesh->getPointCount());
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const Vector<T>& values, T factor = 1.0)
    {
        if (mesh == nullptr) return;
        if (values.size() != mesh->getBorderPointCount(on)) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const T* values, T factor = 1.0)
    {
        if (mesh == nullptr) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(Matrix<T>& op) const
    {
        return;
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(SDMatrix<T>& op) const
    {
        return;
    }

    /**
     * \brief Resets boundary condition (ie sets 0 everywhere)
     */
    void reset()
    {
        //conditions = Vector<T>(conditions.size(), T{});
        for (std::size_t i = 0; i < conditions.size(); i++)
        {
            conditions[i] = T();
        }
    }

    const Vector<T>& getConditions() const
    {
        return conditions;
    }
    /**
     * \brief Apply boundary conditions on the right hand side
     *
     * \param[in, out] rhs The right hand side
     */
    void applyOnRhs(Vector<T>& vec) const
    {
        vec += conditions;
    }
private:
    Vector<T> conditions;
    Grid1D<T>* mesh;
};


/**
 * \brief Dirichlet condition on 2D Grid Mesh
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class DirichletConditionsGrid2D : BoundaryConditions<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param m The grid to apply conditions on
     */
    DirichletConditionsGrid2D(Grid2D<T>* m)
    {
        mesh = m;
        if (mesh != nullptr)
            conditions = Vector<T>(mesh->getPointCount());
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const Vector<T>& values, T factor = 1.0)
    {
        if (mesh == nullptr) return;
        if (values.size() != mesh->getBorderPointCount(on)) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const T* values, T factor = 1.0)
    {
        if (mesh == nullptr) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(Matrix<T>& mat) const
    {
        if (mesh == nullptr) return;

        /* Exclude last index because although it belongs to te boder */
        /* it is the end of the array */
        for (std::size_t i = 0; i < mesh->getBorderPointCount("right") - 1; i++)
        {
            const std::size_t idx = mesh->getBorderPoint("right", i);
            mat[idx][idx + 1] = 0.0;
            mat[idx + 1][idx] = 0.0;
        }
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(SDMatrix<T>& mat) const
    {
        if (mesh == nullptr) return;

        const std::size_t* offsets = mat.getOffsets();
        const std::size_t  offsetsCount = mat.getDiagonalCount();

        /* Exclude last index because although it belongs to te boder */
        /* it is the end of the array */
        const auto it_x = std::find(offsets, offsets + offsetsCount, 1);
        if (it_x != offsets + offsetsCount)
        {
            const auto idx_diag = std::distance(offsets, it_x);
            for (std::size_t i = 0; i < mesh->getBorderPointCount("right") - 1; i++)
            {
                std::size_t idx = mesh->getBorderPoint("right", i);
                mat[idx_diag][idx] = 0.0;
            }
        }
    }

    /**
     * \brief Resets boundary condition (ie sets 0 everywhere)
     */
    void reset()
    {
        conditions = Vector<T>(conditions.size(), T{});
    }

    const Vector<T>& getConditions() const
    {
        return conditions;
    }
    /**
     * \brief Apply boundary conditions on the right hand side
     *
     * \param[in, out] rhs The right hand side
     */
    void applyOnRhs(Vector<T>& vec) const
    {
        vec += conditions;
    }
private:
    Vector<T> conditions;
    Grid2D<T>* mesh;
};


/**
 * \brief Dirichlet condition on 3D Grid Mesh
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class DirichletConditionsGrid3D : public BoundaryConditions<T>
{
public:
    /**
     * \brief Ctor
     *
     * \param m The grid to apply conditions on
     */
    DirichletConditionsGrid3D(Grid3D<T>* m)
    {
        mesh = m;
        if (mesh != nullptr)
            conditions = Vector<T>(mesh->getPointCount());
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const Vector<T>& values, T factor = 1.0)
    {
        if (mesh == nullptr) return;
        if (values.size() != mesh->getBorderPointCount(on)) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Sets the condition on one or more edges
     *
     * \param on The border to sets the condition on
     * \param values The values to set on the border
     * \param factor Additionnal factor to specify
     */
    void setCondition(const std::string& on, const T* values, T factor = 1.0)
    {
        if (mesh == nullptr) return;

        for (std::size_t i = 0; i < mesh->getBorderPointCount(on); i++)
        {
            std::size_t index = mesh->getBorderPoint(on, i);
            conditions[index] += factor * values[i];
        }
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(Matrix<T>& mat) const 
    {
        if (mesh == nullptr) return;
        const std::size_t n = mesh->getXDimension();

        /* Exclude last index because although it belongs to te boder */
        /* it is the end of the array */
        for (std::size_t i = 0; i < mesh->getBorderPointCount("right") - 1; i++)
        {
            const std::size_t idx = mesh->getBorderPoint("right", i);
            mat[idx][idx + 1] = 0.0;
            mat[idx + 1][idx] = 0.0;
        }

        /* Exclude last index because although it belongs to te boder */
        /* it is the end of the array */
        for (std::size_t i = 0; i < mesh->getBorderPointCount("back") - 1; i++)
        {
            const std::size_t idx = mesh->getBorderPoint("back", i);
            mat[idx][idx + n] = 0.0;
            mat[idx + n][idx] = 0.0;
        }
    }

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    void applyOnLinearOperator(SDMatrix<T>& mat) const
    {
        if (mesh == nullptr) return;

        const std::size_t* offsets      = mat.getOffsets();
        const std::size_t  offsetsCount = mat.getDiagonalCount();

        /* Exclude last index because although it belongs to te boder */
        /* it is the end of the array */
        const auto it_x = std::find(offsets, offsets + offsetsCount, 1);
        if (it_x != offsets + offsetsCount)
        {
            const auto idx_diag = std::distance(offsets, it_x);
            for (std::size_t i = 0; i < mesh->getBorderPointCount("right") - 1; i++)
            {
                std::size_t idx = mesh->getBorderPoint("right", i);
                mat[idx_diag][idx] = 0.0;
            }
        }

        const auto it_y = std::find(offsets, offsets + offsetsCount, mesh->getXDimension());
        if (it_y != offsets + offsetsCount)
        {
            const auto idx_diag = std::distance(offsets, it_y);
            for (std::size_t i = 0; i < mesh->getBorderPointCount("front") - mesh->getXDimension(); i++)
            {
                std::size_t idx = mesh->getBorderPoint("front", i);
                mat[idx_diag][idx] = 0.0;
            }
        }
    }

    /**
     * \brief Resets boundary condition (ie sets 0 everywhere)
     */
    void reset()
    {
        //conditions = Vector<T>(conditions.size(), T{});
        for (std::size_t i = 0; i < conditions.size(); i++)
        {
            conditions[i] = T();
        }
    }

    /**
     * \brief Apply boundary conditions on the right hand side
     *
     * \param[in, out] rhs The right hand side
     */
    void applyOnRhs(Vector<T>& vec) const
    {
        vec += conditions;
    }

    const Vector<T>& getConditions() const
    {
        return conditions;
    }
private:
    Vector<T> conditions;
    Grid3D<T>* mesh;
};