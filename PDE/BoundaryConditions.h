#pragma once

#include "../Linalg/linalg.h"

/**
 * \brief Interface that defines Boundary Conditions
 *
 * \tparam Field (eg float, double, std::complex, ...)
 */
template<typename T>
class BoundaryConditions
{
public:
    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    virtual void applyOnLinearOperator(Matrix<T>& op) const = 0;

    /**
     * \brief Apply boundary conditions on a linear operator
     *
     * \param[in, out] op The linear operator
     */
    virtual void applyOnLinearOperator(SDMatrix<T>& op) const = 0;

    /**
     * \brief Apply boundary conditions on the right hand side
     *
     * \param[in, out] rhs The right hand side
     */
    virtual void applyOnRhs(Vector<T>& rhs) const = 0;

    /**
     * \brief Dctor
     */
    virtual ~BoundaryConditions()
    {

    }
private:
};